import os
import subprocess
import datetime
from slugify import slugify
from django.core.exceptions import ValidationError
from django.urls import reverse
from django.db import models


class Grid(models.Model):
	name = models.CharField(max_length=100, help_text="Grid name (compulsory, used for folder naming)")
	creation_date = models.DateField(auto_now_add=True)
	modification_date = models.DateField(auto_now=True)

	class Meta:
		get_latest_by = "modification_date"
		ordering = ['name']

	def __str__(self):
		return self.name

	def clean(self):
		"""
		grid name must not contain underscores
		"""
		if "_" in self.name:
			raise ValidationError("Grid name must not contain underscore ('_') characters!")


class Organization(models.Model):
	name = models.CharField(max_length=100, help_text="Name of the organization or university (compulsory)")
	creation_date = models.DateField(auto_now_add=True)
	modification_date = models.DateField(auto_now=True)

	class Meta:
		get_latest_by = "modification_date"
		ordering = ['name']
		verbose_name = "Organization / University"
		verbose_name_plural = "Organizations / Universities"

	def __str__(self):
		return self.name


class GeographicRegion(models.Model):
	name = models.CharField(max_length=100, help_text="Name of the geographic region that the dataset covers (compulsory)")
	creation_date = models.DateField(auto_now_add=True)
	modification_date = models.DateField(auto_now=True)

	class Meta:
		get_latest_by = "modification_date"
		ordering = ['name']
		verbose_name = "Geographic Region"

	def __str__(self):
		return self.name


class VariableGroup(models.Model):
	name = models.CharField(max_length=100, help_text="Name of the variable group (compulsory, used for folder naming if this variable group is associated with a dataset as a primary variable group)")
	creation_date = models.DateField(auto_now_add=True)
	modification_date = models.DateField(auto_now=True)

	class Meta:
		get_latest_by = "modification_date"
		ordering = ['name']
		verbose_name = "Variable Group"

	def clean(self):
		"""
		variable group name must not contain underscores
		"""
		if "_" in self.name:
			raise ValidationError("VariableGroup name must not contain underscore ('_') characters!")

	def __str__(self):
		return self.name


class People(models.Model):
	title = models.CharField(max_length=10, blank=True, help_text="Academic title")
	last_name = models.CharField(max_length=100, help_text="Last name (compulsory)")
	first_name = models.CharField(max_length=100, blank=True, help_text="First name")
	iac_user_name = models.CharField(max_length=100, blank=True, help_text="User name used at IAC")
	is_staff = models.BooleanField(default=False, help_text="Is or has this person ever been an IAC LandClim staff member or not?")
	organization = models.ManyToManyField(Organization, blank=True, help_text="Organization(s) that this person is associated with")
	address = models.TextField(max_length=1000, blank=True, help_text="Postal address")
	email = models.EmailField(max_length=254, blank=True, help_text="E-Mail address")
	telephone = models.CharField(max_length=100, blank=True, help_text="Telephone number (include the international prefix)")
	fax = models.CharField(max_length=100, blank=True, help_text="Fax number")
	creation_date = models.DateField(auto_now_add=True)
	modification_date = models.DateField(auto_now=True)
	
	class Meta:
		ordering = ['last_name', 'first_name']
		verbose_name_plural = "people"
	
	def __str__(self):
		return u'%s %s' % (self.first_name, self.last_name)


class SupDataset(models.Model):
	# unique identifier
	key = models.SlugField(editable=False)

	# dataset name
	short_name = models.CharField(max_length=100, unique=True, help_text="Dataset abbreviation (compulsory, used for folder naming)") # !
	full_name = models.CharField(max_length=1000, help_text="Full (official) dataset name (compulsory)")
	# data contact
	organization = models.ForeignKey(Organization, on_delete=models.SET_NULL, blank=True, null=True, help_text="Main organization that provides the dataset")
	contact = models.ForeignKey(People, on_delete=models.SET_NULL, blank=True, null=True, limit_choices_to={'is_staff': False}, help_text="Main dataset contact person")	
	main_user = models.ForeignKey(People, on_delete=models.SET_NULL, blank=True, null=True, related_name='supdataset_user', limit_choices_to={'is_staff': True}, default=People.objects.get(iac_user_name="hirschim").pk, help_text="Staff member that is the main user of the sub-dataset or that has the highest experience with the sub-dataset")
	# access restrictions
	special_restrictions = models.NullBooleanField(default=False, help_text="Does the sub-dataset underly a particularly restrictive data policy? (in this case, data belonging to this dataset can only be accessed by contacting the main user of the dataset)")
	registration_required = models.NullBooleanField(default=False, help_text="Should users of the sub-dataset register at the data provider website when using it for publications?")

	# data description and documentation, and associated links - if this is not supplied, the content of any of these fields will be automatically derived from the content of the related sup-dataset
	summary = models.TextField(blank=True, help_text="Summary text of this dataset; this can correspond to the abstract of the main publication")
	publication_doi = models.CharField(max_length=500, blank=True, help_text="DOI of the main publication that describes the dataset")
	dataset_doi = models.CharField(max_length=500, blank=True, help_text="DOI of the dataset (e.g., datasets published via Pangea always have their own DOI)")
	project_url = models.URLField(blank=True,max_length=500, help_text="URL of the project website (the website that provides the dataset)")
	data_source_url = models.URLField(blank=True,max_length=500, help_text="URL of the website where the data can be directly obtained or URL of the website where users can register or log in")
	known_issues = models.TextField(blank=True, help_text="Short text describing known issues related to the dataset")
	known_issues_link = models.CharField(max_length=500, blank=True, help_text="File name of the document (e.g., pdf file) describing known issues related to the dataset; if you provide this information, please remember to also download the respective file to the doc directory of the related sup-dataset", verbose_name="Known issues file name")
	data_policy = models.TextField(blank=True, help_text="Short text describing the data policy valid for the dataset")
	data_policy_link = models.CharField(max_length=500, blank=True, help_text="File name of the document (e.g., pdf file) describing the data policy valid for the dataset; if you provide this information, please remember to also download the respective file to the doc directory of the related sup-dataset", verbose_name="Data policy file name")
	manual_link = models.CharField(max_length=500, blank=True, help_text="File name of the dataset manual (e.g., pdf file); if you provide this information, please remember to also download the respective file to the doc directory of the related sup-dataset", verbose_name="Manual file name")
	quick_user_guide_link = models.CharField(max_length=500, blank=True, help_text="File name of the dataset quick user guide (e.g., pdf file); if you provide this information, please remember to also download the respective file to the doc directory of the related sup-dataset", verbose_name="Quick user guide file name")
	technical_manual_link = models.CharField(max_length=500, blank=True, help_text="File name of the dataset technical manual (e.g., pdf file); if you provide this information, please remember to also download the respective file to the doc directory of the related sup-dataset", verbose_name="Technical manual file name")	
	
	# data origin
	data_origin_category_choices = (
		('composite','composite'),
		('diagnostic / index','diagnostic / index'),
		('digital elevation model','digital elevation model'),
		('disaster','disaster'),
		('in situ','in situ'),
		('in situ gridded','in situ gridded'),
		('land cover / land use','land cover / land use'),
		('model output','model output'),
		('population','population'),
		('reanalysis','reanalysis'),
		('remote sensing','remote sensing')
	)
	data_origin_category = models.CharField(max_length=100, choices=data_origin_category_choices, blank=True, default='none', help_text="Category of the data (e.g., in-situ, reanalysis, model data, ...)", verbose_name="Data category")
	source_dataset = models.CharField(max_length=1000, blank=True, help_text="Dataset that this sub-dataset is derived from or that this sub-dataset is built upon")
	source_processing = models.CharField(max_length=10000, blank=True, help_text="Processing steps applied to generate this dataset", verbose_name="Processing applied")
	
	# settings required to generate IAC-wide data repository entry (below /net/atmos/data) and associated short documentation
	has_iac_read_access = models.BooleanField(blank=False, null=False, default=False, help_text="Grant read access to this dataset to all IAC members. CAUTION: When removing this flag, you have to manually remove the read permissions for all non-landclim users as follows: cd /net/atmos/data/; chmod -R o-r <dataset_name>")
	is_linked_to_net_atmos = models.BooleanField(verbose_name="is linked to /net/atmos/data", blank=False, null=False, default=False, help_text="Link this dataset to /net/atmos/data/ (only do this for datasets that are very commonly used at IAC)")
	iac_readme_summary = models.TextField(blank=True, null=True, default="", help_text="Short summary of the dataset that is going to be shown in /net/atmos/data/<dataset_name>/README.txt")
	iac_readme_contact = models.CharField(max_length=200, blank=True, null=True, default="", help_text="Contact information that is going to be shown in /net/atmos/data/<dataset_name>/README.txt")
	iac_readme_links = models.TextField(blank=True, null=True, default="", help_text="Links related to the dataset that are going to be shown in /net/atmos/data/<dataset_name>/README.txt")
	
	# non-editable
	creation_date = models.DateField(auto_now_add=True, editable=False)
	modification_date = models.DateField(auto_now=True, editable=False)	

	class Meta:
		get_latest_by = "modification_date"
		ordering = ['short_name']
		verbose_name = "Dataset"
	
	def clean(self):
		if "_" in self.short_name:
			self.short_name = self.short_name.replace("_","-")
		self.key = slugify(self.short_name)
		if self.key == "dataset" or self.key == "variable":
			raise ValidationError("The SupDataset must not be named 'dataset' or 'variable'!")
		
		is_updating = True
		if len(SupDataset.objects.filter(pk=self.pk)) != 0:
			sup_old = SupDataset.objects.get(pk=self.pk)

			if sup_old.is_linked_to_net_atmos != self.is_linked_to_net_atmos:				
				if not self.subdataset_set.count():
					self.subdataset_set.first().save()
			else:
				is_updating = False		

		if is_updating:
			data_roots = []
			for sub in self.subdataset_set.all():
				data_roots.append(sub.data_root)

			data_roots = list(set(data_roots))

			if self.is_linked_to_net_atmos:
				if not self.has_iac_read_access:
					raise ValidationError("When linking this dataset to /net/atmos/data, you also have to grant IAC read permissions to it.")
				if not self.iac_readme_summary:
					raise ValidationError("When linking this dataset to /net/atmos/data, you also have to provide a summary text for the README.")
				if not self.iac_readme_contact:
					raise ValidationError("When linking this dataset to /net/atmos/data, you also have to provide a contact info text for the README.")					
				if self.iac_readme_summary and self.iac_readme_contact:
					for data_root in data_roots:
						f = open(os.path.join(data_root.replace('/net/exo/landclim/data/dataset','/nfs/exo/landclim/dataset'),"README.txt"), 'w')
						f.write("[Summary]\n")
						f.write(self.iac_readme_summary+"\n\nPlease acknowledge the LandClim group when using data from this directory for your publications.\n\n")
						f.write("[Contact]\n")
						if not self.iac_readme_links:
							f.write(self.iac_readme_contact+"\n")
						if self.iac_readme_links:
							f.write(self.iac_readme_contact+"\n\n")
							f.write("[Links]\n")
							f.write(self.iac_readme_links+"\n")
						f.close()
						link_exists = os.path.islink(os.path.join("/nfs/atmos/data/",data_root.split("/")[-2]))
						if not link_exists:
							cmd = "cd /nfs/atmos/data/; ln -s " + data_root
							os.system(cmd)
			else:
				for data_root in data_roots:
					link_exists = os.path.islink(os.path.join("/nfs/atmos/data/",data_root.split("/")[-2]))
					if link_exists:
						cmd = "cd /nfs/atmos/data/; rm " + data_root.split("/")[-2]
						os.system(cmd)
		
		is_updating = True
		if len(SupDataset.objects.filter(pk=self.pk)) != 0:
			if (sup_old.has_iac_read_access != self.has_iac_read_access):
				if not self.subdataset_set.count():
					self.subdataset_set.first().save()
			else:
				is_updating = False

		if is_updating:
			data_roots = []
			for sub in self.subdataset_set.all():
				data_roots.append(sub.data_root)

			data_roots = list(set(data_roots))

			if self.has_iac_read_access:
				for data_root in data_roots:				
					cmd = "cd " + data_root.replace('/net/exo/landclim/data/dataset','/nfs/exo/landclim/dataset') + "; chmod o+rx ." # assign read + executable permissions to 'others' for the data root directory
					os.system(cmd)
					cmd = "cd " + data_root.replace('/net/exo/landclim/data/dataset','/nfs/exo/landclim/dataset') + "; chmod -R o+r *" # assign read permissions to 'others' for all files and sub-directories contained in the data root directory
					os.system(cmd)
					cmd = 'cd ' + data_root.replace("/net/exo/landclim/data/dataset","/nfs/exo/landclim/dataset") + '; find . -type d -exec chmod o+x {} \;' # assign executable permissions to 'others' for all sub-directories contained in the data root directory
					os.system(cmd)					
					# cmd = 'cd ' + data_root.replace("/net/exo/landclim/data/dataset","/nfs/exo/landclim/dataset") + '; find . -name "processed" -type d -exec chmod -R o-rx {} \;' # remove read and executable permissions from 'others' for all 'processed' sub-directories and all sub-directories and files therein
					# os.system(cmd) # do not exclude processed folders from /net/atmos/data/...
			else:
				if self.is_linked_to_net_atmos:
					self.is_linked_to_net_atmos = False # if there is no read access for all, automatically reset is_linked_to_net_atmos to False

				for data_root in data_roots:
					cmd = "cd " + data_root.replace('/net/exo/landclim/data/dataset','/nfs/exo/landclim/dataset') + "; chmod -R o-rx *" # remove read and executable permissions from 'others' for all files and sub-directories contained in the data root directory
					os.system(cmd)
					cmd = "cd " + data_root.replace('/net/exo/landclim/data/dataset','/nfs/exo/landclim/dataset') + "; chmod o-rx ." # remove read + executable permissions from 'others' for the data root directory
					os.system(cmd)

	def __str__(self):
		return self.short_name

	def get_absolute_url(self):
		return reverse('supdataset-detail', kwargs={'slug': self.key})


class SubDataset(models.Model):
	# related supdataset, subdataset name and version
	supdataset = models.ForeignKey(SupDataset, on_delete=models.SET_NULL, null=True, blank=True, help_text="Sup-dataset that this sub-dataset belongs to")
	name = models.CharField(max_length=1000, null=True, blank=True, help_text="Name of sub-dataset (used for folder naming)")
	download_date = models.DateField(max_length=20, null=True, blank=True, default=datetime.date.today(), help_text="Date at which the download has been completed (YYYY-MM-DD; used for folder naming)")
	version = models.CharField(max_length=20, null=True, blank=True, help_text="Official version identifier (used for folder naming)")
	link_location = models.CharField(max_length=1000, null=True, blank=True, help_text="Path to physical location of this sub-dataset (only provide this, if the sub-dataset is stored outside the data repository; used for folder naming)")

	# remote sensing specific parameters
	processing_level = models.CharField(max_length=20, null=True, blank=True, help_text="Remote sensing data only: Processing level that this sub-dataset corresponds to (used for folder naming)")
	band = models.CharField(max_length=10, null=True, blank=True, help_text="Remote sensing data only: Band that this sub-dataset corresponds to (used for folder naming)")
	orbit_direction = models.CharField(max_length=10, null=True, blank=True, help_text="Remote sensing data only: Orbit direction that this sub-dataset corresponds to (used for folder naming)")

	# main variables
	primary_variable_groups = models.ManyToManyField(VariableGroup, related_name='primary_variable_groups', blank=True, default='', help_text="Primary variable group(s) associated with this subdataset (used for folder naming)")

	# sample file used to derive detailed parameters
	sample_file = models.CharField(max_length=1000, null=True, blank=True, default=None, help_text="Sample NetCDF file stored within the data repository consisting of the entire grid and containing all time steps. The file name should be provided with its absolute path (/net/exo/.../file.nc). Upon saving this model, the selected file will be parsed and spatial parameters will be automatically extracted whenever possible. When successful, the extracted parameters overwrite any user-supplied spatio-temporal coverage parameters.")

	# file format
	file_format = models.CharField(max_length=100, blank=True, help_text="File format of the original data (e.g., NetCDF, grib, ascii)") # TBA

	# spatial coverage parameters
	geographic_region = models.ForeignKey(GeographicRegion, on_delete=models.SET_NULL, blank=True, null=True, default=GeographicRegion.objects.get(name="global land+ocean").pk, help_text="Geographic region that the sub-dataset covers") # TBA
	bounding_latitude_north = models.DecimalField(max_digits=5,decimal_places=3, blank=True, null=True, default=-90.0, help_text="Northern boundary of the sub-dataset [degrees latitude]") # TBA
	bounding_longitude_west = models.DecimalField(max_digits=7,decimal_places=3, blank=True, null=True, default=-180.0, help_text="Western boundary of the sub-dataset [degrees longitude]") # TBA
	bounding_latitude_south = models.DecimalField(max_digits=5,decimal_places=3, blank=True, null=True, default=90.0, help_text="Southern boundary of the sub-dataset [degrees latitude]") # TBA
	bounding_longitude_east = models.DecimalField(max_digits=7,decimal_places=3, blank=True, null=True, default=180.0, help_text="Eastern boundary of the sub-dataset [degrees longitude]") # TBA

	# temporal coverage parameters
	time_span_start = models.DateField(blank=True, null=True, help_text="Start of time span covered by the dataset realization") # TBA
	time_span_end = models.DateField(blank=True, null=True, help_text="End of time span covered by the dataset realization") # TBA

	# data description and documentation, and associated links - if this is not supplied, the content of any of these fields will be automatically derived from the content of the related sup-dataset
	summary = models.TextField(blank=True, help_text="Summary text of the sub-dataset; this can correspond to the abstract of the main publication")
	publication_doi = models.CharField(max_length=500, blank=True, help_text="DOI of the main publication that describes the sub-dataset")
	dataset_doi = models.CharField(max_length=500, blank=True, help_text="DOI of the sub-dataset (e.g., datasets published via Pangea always have their own DOI)")
	project_url = models.URLField(blank=True,max_length=500, help_text="URL of the project website (the website that provides the sub-dataset)")
	data_source_url = models.URLField(blank=True,max_length=500, help_text="URL of the website where the data can be directly obtained (no login required) or URL of the website where users can register or log in (login required)")
	known_issues = models.TextField(blank=True, help_text="Short text describing known issues related to the sub-dataset")
	known_issues_link = models.CharField(max_length=500, blank=True, help_text="File name of the document (e.g., pdf file) describing known issues related to the sub-dataset; if you provide this information, please remember to also download the respective file to the doc directory of the related sup-dataset", verbose_name="Known issues file name")
	data_policy = models.TextField(blank=True, help_text="Short text describing the data policy valid for the sub-dataset")
	data_policy_link = models.CharField(max_length=500, blank=True, help_text="File name of the document (e.g., pdf file) describing the data policy valid for the sub-dataset; if you provide this information, please remember to also download the respective file to the doc directory of the related sup-dataset", verbose_name="Data policy file name")
	manual_link = models.CharField(max_length=500, blank=True, help_text="File name of the sub-dataset manual (e.g., pdf file); if you provide this information, please remember to also download the respective file to the doc directory of the related sup-dataset", verbose_name="Manual file name")
	quick_user_guide_link = models.CharField(max_length=500, blank=True, help_text="File name of the sub-dataset quick user guide (e.g., pdf file); if you provide this information, please remember to also download the respective file to the doc directory of the related sup-dataset", verbose_name="Quick user guide file name")
	technical_manual_link = models.CharField(max_length=500, blank=True, help_text="File name of the sub-dataset technical manual (e.g., pdf file); if you provide this information, please remember to also download the respective file to the doc directory of the related sup-dataset", verbose_name="Technical manual file name")

	# data origin
	data_origin_category_choices = (
		('composite','composite'),
		('diagnostic / index','diagnostic / index'),
		('digital elevation model','digital elevation model'),
		('disaster','disaster'),
		('in situ','in situ'),
		('in situ gridded','in situ gridded'),
		('land cover / land use','land cover / land use'),
		('model output','model output'),
		('population','population'),
		('reanalysis','reanalysis'),
		('remote sensing','remote sensing'),
	)
	data_origin_category = models.CharField(max_length=100, choices=data_origin_category_choices, blank=True, default='none', help_text='Category of the data (e.g., in-situ, reanalysis, model data, ...)', verbose_name='Data category')
	source_dataset = models.CharField(max_length=1000, blank=True, help_text="Dataset that this sub-dataset is derived from or that this sub-dataset is built upon")
	source_processing = models.CharField(max_length=10000, blank=True, help_text="Processing steps applied to generate this dataset", verbose_name="Processing applied")

	# non-editable
	data_root = models.TextField(default="", editable=False)
	readme_file = models.TextField(default="", editable=False)
	creation_date = models.DateField(auto_now_add=True, editable=False)
	modification_date = models.DateField(auto_now=True, editable=False)	

	@property
	def all_primary_variable_groups(self):
		return ', '.join([x.name for x in self.primary_variable_groups.all()])

	class Meta:
		get_latest_by = "modification_date"

	def clean(self):
		"""
		subdataset name, version, link_location, processing_level, band, orbit_direction must not contain underscores
		"""
		if self.processing_level=="":
			self.processing_level = None

		if self.band=="":
			self.band = None

		if self.orbit_direction=="":
			self.orbit_direction = None

		if self.version=="":
			self.version = None

		if self.name=="":
			self.name = None

		if self.download_date=="":
			self.download_date = None

		if self.link_location=="":
			self.link_location = None

		if self.name:
			if "_" in self.name:
				raise ValidationError("SubDataset name must not contain underscore ('_') characters!")

		if self.version:
			if "_" in self.version:
				raise ValidationError("SubDataset version must not contain underscore ('_') characters!")

		if self.processing_level:
			if "_" in self.processing_level:
				raise ValidationError("SubDataset processing level must not contain underscore ('_') characters!")

		if self.band:
			if "_" in self.band:
				raise ValidationError("SubDataset band must not contain underscore ('_') characters!")

		if self.orbit_direction:
			if "_" in self.orbit_direction:
				raise ValidationError("SubDataset orbit direction must not contain underscore ('_') characters!")

		if self.sample_file:			
			if not os.path.isfile(self.sample_file.replace('/net/exo/landclim/data/dataset','/nfs/exo/landclim/dataset').replace('/net/exo/landclim/data/variable','/nfs/exo/landclim/variable')):
				raise ValidationError("The file path " + self.sample_file + " is invalid.")

		"""
		Require either version or download_date
		"""
		if not (self.version or self.download_date):
			raise ValidationError("Provide either the version or the download date (or both)!")
		if (self.bounding_latitude_north or self.bounding_latitude_south or self.bounding_longitude_east or self.bounding_longitude_west):
			if not (self.bounding_latitude_north or self.bounding_latitude_south or self.bounding_longitude_east or self.bounding_longitude_west):
				raise ValidationError("Provide either all four lat/lon boundaries or none!")

		if self.name and len(self.name)>0:
			if self.processing_level and self.band and self.orbit_direction:
				self.data_root = "/net/exo/landclim/data/dataset/"+self.supdataset.short_name.replace(" ","-")+"_"+self.name.replace(" ","-")+"_L"+self.processing_level+"_"+self.band+"-BAND_"+self.orbit_direction+"/"
			elif self.processing_level and not self.band and not self.orbit_direction:
				self.data_root = "/net/exo/landclim/data/dataset/"+self.supdataset.short_name.replace(" ","-")+"_"+self.name.replace(" ","-")+"_L"+self.processing_level+"/"
			elif self.processing_level and self.band and not self.orbit_direction:
				self.data_root = "/net/exo/landclim/data/dataset/"+self.supdataset.short_name.replace(" ","-")+"_"+self.name.replace(" ","-")+"_L"+self.processing_level+"_"+self.band+"-BAND/"
			elif self.processing_level and not self.band and self.orbit_direction:
				self.data_root = "/net/exo/landclim/data/dataset/"+self.supdataset.short_name.replace(" ","-")+"_"+self.name.replace(" ","-")+"_L"+self.processing_level+"_"+self.orbit_direction+"/"
			elif not self.processing_level and self.band and self.orbit_direction:
				self.data_root = "/net/exo/landclim/data/dataset/"+self.supdataset.short_name.replace(" ","-")+"_"+self.name.replace(" ","-")+"_"+self.band+"-BAND_"+self.orbit_direction+"/"
			elif not self.processing_level and not self.band and self.orbit_direction:
				self.data_root = "/net/exo/landclim/data/dataset/"+self.supdataset.short_name.replace(" ","-")+"_"+self.name.replace(" ","-")+"_"+self.orbit_direction+"/"
			elif not self.processing_level and self.band and not self.orbit_direction:
				self.data_root = "/net/exo/landclim/data/dataset/"+self.supdataset.short_name.replace(" ","-")+"_"+self.name.replace(" ","-")+"_"+self.band+"-BAND/"
			elif not self.processing_level and not self.band and not self.orbit_direction:
				self.data_root = "/net/exo/landclim/data/dataset/"+self.supdataset.short_name.replace(" ","-")+"_"+self.name.replace(" ","-")+"/"
		else:
			if self.processing_level and self.band and self.orbit_direction:
				self.data_root = "/net/exo/landclim/data/dataset/"+self.supdataset.short_name.replace(" ","-")+"_L"+self.processing_level+"_"+self.band+"-BAND_"+self.orbit_direction+"/"
			elif self.processing_level and not self.band and not self.orbit_direction:
				self.data_root = "/net/exo/landclim/data/dataset/"+self.supdataset.short_name.replace(" ","-")+"_L"+self.processing_level+"/"
			elif self.processing_level and self.band and not self.orbit_direction:
				self.data_root = "/net/exo/landclim/data/dataset/"+self.supdataset.short_name.replace(" ","-")+"_L"+self.processing_level+"_"+self.band+"-BAND/"
			elif self.processing_level and not self.band and self.orbit_direction:
				self.data_root = "/net/exo/landclim/data/dataset/"+self.supdataset.short_name.replace(" ","-")+"_L"+self.processing_level+"_"+self.orbit_direction+"/"
			elif not self.processing_level and self.band and self.orbit_direction:
				self.data_root = "/net/exo/landclim/data/dataset/"+self.supdataset.short_name.replace(" ","-")+"_"+self.band+"-BAND_"+self.orbit_direction+"/"
			elif not self.processing_level and not self.band and self.orbit_direction:
				self.data_root = "/net/exo/landclim/data/dataset/"+self.supdataset.short_name.replace(" ","-")+"_"+self.orbit_direction+"/"
			elif not self.processing_level and self.band and not self.orbit_direction:
				self.data_root = "/net/exo/landclim/data/dataset/"+self.supdataset.short_name.replace(" ","-")+"_"+self.band+"-BAND/"
			elif not self.processing_level and not self.band and not self.orbit_direction:
				self.data_root = "/net/exo/landclim/data/dataset/"+self.supdataset.short_name.replace(" ","-")+"/"

		if self.name and len(self.name)>0:
			if self.version and len(self.version)>0:
				self.readme_file = os.path.join(self.data_root,"README_"+slugify(self.name)+"-"+slugify(self.version)+".html")
			else:
				self.readme_file = os.path.join(self.data_root,"README_"+slugify(self.name)+".html")
		else:
			if self.version and len(self.version)>0:			
				self.readme_file = os.path.join(self.data_root,"README_"+slugify(self.version)+".html")
			else:
				self.readme_file = os.path.join(self.data_root,"README_"+slugify(str(self.download_date))+".html")

	def __str__(self):
		if not self.name:
			name = ""
		else:
			name = self.name

		if not self.version:
			version = ""
		else:
			version = self.version

		if not self.download_date:
			download_date = ""
		else:
			download_date = self.download_date			

		if not self.band:
			band = ""
		else:
			band = self.band

		if not self.processing_level:
			processing_level = ""
		else:
			processing_level = self.processing_level

		if not self.orbit_direction:
			orbit_direction = ""
		else:
			orbit_direction = self.orbit_direction

		if self.link_location:
			return u'%s %s %s %s %s %s (linked)' % (name, version, download_date, processing_level, band, orbit_direction)
		else:
			return u'%s %s %s %s %s %s' % (name, version, download_date, processing_level, band, orbit_direction)


class DatasetRealization(models.Model):
	# unique identifier
	key = models.SlugField(null=False, unique=True, editable=False, max_length=500)

	# relationships to other models
	subdataset = models.ForeignKey(SubDataset, on_delete=models.SET_NULL, blank=True, null=True, help_text="Sub-dataset that this dataset realization belongs to")

	# data type classification
	is_model = models.BooleanField(default=False,blank=True, help_text="Does this dataset realization correspond to model output that contains its own folder structure or contains many different realizations (e.g., multiple spatiotemporal resolutions)? If yes, any information related to the resolution and grid must not be provided (used for folder naming)")
	is_mixed = models.BooleanField(default=False,blank=True, help_text="Does this dataset realization contain multiple spatial and temporal resolutions, but is not model output? (used for folder naming)")

	# spatial resolution information
	spatial_resolution_lon = models.DecimalField(max_digits=6, decimal_places=3, blank=True, null=True, help_text="Longitudinal resolution (used for folder naming)")
	spatial_resolution_lat = models.DecimalField(max_digits=6, decimal_places=3, blank=True, null=True, help_text="Latitudinal resolution (used for folder naming)")
	spatial_resolution_unit_choices = (
		('deg','deg: °'),
		('km','km: kilometers'),
		('m','m: meters'),
		('min','min: arc-minutes'),
		('sec','sec: arc-seconds')
	)
	spatial_resolution_unit = models.CharField(max_length=15, choices=spatial_resolution_unit_choices, default='none',blank=True,null=True, help_text="Unit of the spatial resolution (used for folder naming)")
	country_scale = models.BooleanField(default=False,blank=True, help_text="The dataset realization corresponds to individual countries (used for folder naming)")	
	basin_scale = models.BooleanField(default=False,blank=True, help_text="The dataset realization corresponds to individual basins (used for folder naming)")
	point_scale = models.BooleanField(default=False,blank=True, help_text="The dataset realization corresponds to unstructured point data (used for folder naming)")	
	grid = models.ForeignKey(Grid, on_delete=models.SET_NULL, blank=True, null=True, help_text="Name of the grid (used for folder naming)")

	# temporal resolution information
	temporal_resolution = models.DecimalField(max_digits=3,decimal_places=1,blank=True,null=True, help_text="Temporal resolution (used for folder naming)")	
	temporal_resolution_unit_choices = (
		('p','p: pentad'),
		('y','y: year'),
		('s','s: season'),
		('m','m: month'),
		('d','d: day'),
		('h','h: hour'),
		('m ltm','m ltm: long-term monthly average'),
		('d ltm','d ltm: long-term daily average'),
		('h mm','h ltm: monthly means of hourly average')
	)
	temporal_resolution_unit = models.CharField(max_length=10, choices=temporal_resolution_unit_choices, default='none', blank=True, null=True, help_text="Unit of the temporal resolution (used for folder naming)")
	irregular = models.BooleanField(default=False, help_text="Time dimension is irregular (used for folder naming)")
	time_invariant = models.BooleanField(default=False, help_text="There is no time dimension (i.e., the data is static; used for folder naming)")

	# non-editable
	creation_date = models.DateField(auto_now_add=True, editable=False)
	modification_date = models.DateField(auto_now=True, editable=False)
	
	class Meta:
		get_latest_by = "modification_date"
		verbose_name = "dataset realization"
		verbose_name_plural = "dataset realizations"

	def clean(self):
		"""
		dataset_realization spatial_resolution_unit, temporal_resolution_unit must not contain underscores
		"""
		if self.spatial_resolution_unit:
			if "_" in self.spatial_resolution_unit:
				raise ValidationError("Spatial resolution units must not contain underscore ('_') characters!")

		if self.temporal_resolution_unit:
			if "_" in self.temporal_resolution_unit:
				raise ValidationError("Temporal resolution units must not contain underscore ('_') characters!")

		parts = (str(self.spatial_resolution_lon), str(self.spatial_resolution_lat), self.spatial_resolution_unit)
		if self.is_model:
			if (self.spatial_resolution_lat or self.spatial_resolution_lon or self.country_scale or self.basin_scale or self.point_scale or self.temporal_resolution or self.temporal_resolution_unit or self.grid or self.is_mixed):
				raise ValidationError("If you indicated that this is a model, any information regarding the grid or the resolution is superfluous (model data are saved in their own intrinsic folder structure).")
		elif self.is_mixed:
			if (self.spatial_resolution_lat or self.spatial_resolution_lon or self.country_scale or self.basin_scale or self.point_scale or self.temporal_resolution or self.temporal_resolution_unit or self.grid or self.is_model):
				raise ValidationError("If you indicated that this dataset realization is mixed (i.e., contains multiple spatial or temporal resolutions but is not a model), any information regarding the grid or the resolution is superfluous.")
		else:
			"""
			Require either spatial_resolution_*, country_scale or basin_scale or point_scale
			"""
			if (self.spatial_resolution_lon or self.spatial_resolution_lat) and not (self.spatial_resolution_lon and self.spatial_resolution_lat and self.spatial_resolution_unit):
				raise ValidationError("Provide both longitudinal and latitudinal resolution and the corresponding unit or none of these items!")
			if not (self.spatial_resolution_lon and self.spatial_resolution_lat and self.spatial_resolution_unit):
				if not (self.country_scale or self.basin_scale or self.point_scale):
					raise ValidationError("Provide either spatial resolution or indicate that the data represents either countries, basins or points!")
			elif (self.spatial_resolution_lon and self.spatial_resolution_lat and self.spatial_resolution_unit):
				if self.country_scale or self.basin_scale or self.point_scale:
					raise ValidationError("Provide either spatial resolution or indicate that the data represents either countries, basins or points!")
			"""
			Require grid
			"""
			if not self.grid:
				raise ValidationError("Please provide the name of the grid, or 'none' if the data is not gridded.")

			"""
			Require either temporal_resolution_* or irregular or time invariant
			"""
			if not (self.temporal_resolution or self.irregular or self.time_invariant):
				raise ValidationError("Provide temporal resolution or indicate that the data is irregularly sampled or time-invariant!")
			else:
				if ((self.temporal_resolution and self.irregular and self.time_invariant) or (self.temporal_resolution and self.irregular) or (self.temporal_resolution and self.time_invariant) or (self.irregular and self.time_invariant)):
					raise ValidationError("Provide either temporal resolution or indicate that the data is either irregularly sampled or time-invariant!")
				else:
					if self.temporal_resolution:
						if not (self.temporal_resolution and self.temporal_resolution_unit):
							raise ValidationError("Provide both temporal resolution and the corresponding unit!")

		self.key = slugify(str(self.subdataset.supdataset)+"_"+str(self.subdataset)+"_"+str(self.basin_scale)+"_"+str(self.country_scale)+"_"+str(self.grid)+"_"+str(self.irregular)+"_"+str(self.is_model)+"_"+str(self.is_mixed)+"_"+str(self.point_scale)+"_"+str(self.spatial_resolution_lat)+"_"+str(self.spatial_resolution_lon)+"_"+str(self.spatial_resolution_unit)+"_"+str(self.temporal_resolution)+"_"+str(self.temporal_resolution_unit)+"_"+str(self.time_invariant))

	def __str__(self):
		if (self.is_model==False) and (self.is_mixed==False):
			if self.spatial_resolution_lon or self.spatial_resolution_lat:
				parts =	(str('{0:g}'.format(float(self.spatial_resolution_lon))), "x", str('{0:g}'.format(float(self.spatial_resolution_lat))), str(self.spatial_resolution_unit))
				spatial_resolution = ''.join(str(part) for part in parts if part is not None)
			elif self.country_scale:
				spatial_resolution = "country scale"
			elif self.basin_scale:
				spatial_resolution = "basin scale"
			elif self.point_scale:
				spatial_resolution = "point scale"
			else:
				return u'undefined'
			if self.temporal_resolution:
				temporal_resolution = str('{0:g}'.format(float(self.temporal_resolution)) + self.temporal_resolution_unit)
			elif self.irregular:
				temporal_resolution = "irregular"
			elif self.time_invariant:
				temporal_resolution = "time-invariant"
			else:
				return u'undefined'

			return u'spatial resolution: %s, temporal resolution: %s' % (spatial_resolution, temporal_resolution)

		elif self.is_model:
			return u'model data'

		elif self.is_mixed:
			return u'mixed data'


class ProcessedDatasetRealization(models.Model):
	# unique key
	key = models.SlugField(blank=False, null=False, unique=True, editable=False, max_length=500)

	# relationships to other models
	dataset_realization = models.ForeignKey(DatasetRealization, on_delete=models.CASCADE, blank=False, null=True, help_text="Dataset realization that this processed dataset realization belongs to")

	# has post-processing been applied?
	data_processed = models.BooleanField(default=False, blank=False, null=False, help_text="This processed dataset realization corresponds to data processed from the original version (true) or not (false) (used for folder naming)")
	processing_performed_by = models.ForeignKey(People, on_delete=models.SET_NULL, blank=True, null=True, limit_choices_to={'is_staff': True}, help_text="Staff member who has performed the processing")
	processing_tool = models.CharField(max_length=100, blank=True, null=True, help_text="Tool used to perform the post-processing")
	processing_tool_version = models.CharField(max_length=100, blank=True, null=True, help_text="Version of the tool used to perform the post-processing")
	processing_description = models.CharField(max_length=10000, blank=True, null=True, help_text="Description of the processing")
	
	# available choices
	processing_choices = (
		('anom', 'anom: computed anomalies'),
		('deriv', 'deriv: derived parameter(s)'),
		('ensmean','ensmean: computed ensemble mean'),
		('fixed', 'fixed: fixed error(s) other than file format issues'),
		('geotiff', 'geotiff: fixed geotiff'),
		('netcdf','netcdf: fixed netcdf'),
		('regrid','regrid: regridded to lat-lon grid'),
		('runmean', 'runmean: applied running means (moving averages)'),
	 	('tmax','tmax: aggregated over time by maximizing'),
	 	('tmean','tmean: aggregated over time by averaging'),
		('tmin','tmin: aggregated over time by minimizing'),
	 	('tsum','tsum: aggregated over time by summation'),
	)
	processed_spatial_resolution_unit_choices = (
		('deg','deg: °'),
		('km','km: kilometers'),
		('m','m: meters'),
		('min','min: arc-minutes'),
		('sec','sec: arc-seconds')
	)
	processed_temporal_resolution_unit_choices = (
		('p','p: pentad'),
		('y','y: year'),
		('s','s: season'),
		('m','m: month'),
		('d','d: day'),
		('h','h: hour'),
		('m ltm','m ltm: long-term monthly average'),
		('d ltm','d ltm: long-term daily average'),
		('h mm','h ltm: monthly means of hourly average')
	)
	# processing applied (in chronological order: processing1 > processing2 > processing3)
	processing1 = models.CharField(max_length=100, blank=True, null=True, choices=processing_choices, help_text="Processing applied first (used for folder naming)")
	processing1_spatial_resolution = models.DecimalField(max_digits=6, decimal_places=3, blank=True, null=True, help_text="Resolution (identical in both latitudinal and longitudinal direction; used for folder naming)")
	processing1_spatial_resolution_unit = models.CharField(max_length=15,choices=processed_spatial_resolution_unit_choices,default=None,blank=True,null=True, help_text="Unit of the spatial resolution (used for folder naming)")
	processing1_temporal_resolution = models.DecimalField(max_digits=3,decimal_places=1,blank=True,null=True, help_text="Temporal resolution (used for folder naming)")
	processing1_temporal_resolution_unit = models.CharField(max_length=10,choices=processed_temporal_resolution_unit_choices,default=None,blank=True,null=True, help_text="Unit of the temporal resolution (used for folder naming)")
	processing2 = models.CharField(max_length=100, blank=True, null=True, choices=processing_choices, help_text="Processing applied second (used for folder naming)")
	processing2_spatial_resolution = models.DecimalField(max_digits=6, decimal_places=3, blank=True, null=True, help_text="Resolution (identical in both latitudinal and longitudinal direction; used for folder naming)")
	processing2_spatial_resolution_unit = models.CharField(max_length=15,choices=processed_spatial_resolution_unit_choices,default=None,blank=True,null=True, help_text="Unit of the spatial resolution (used for folder naming)")
	processing2_temporal_resolution = models.DecimalField(max_digits=3,decimal_places=1,blank=True,null=True, help_text="Temporal resolution (used for folder naming)")
	processing2_temporal_resolution_unit = models.CharField(max_length=10,choices=processed_temporal_resolution_unit_choices,default=None,blank=True,null=True, help_text="Unit of the temporal resolution (used for folder naming)")
	processing3 = models.CharField(max_length=100, blank=True, null=True, choices=processing_choices, help_text="Processing applied third (used for folder naming)")
	processing3_spatial_resolution = models.DecimalField(max_digits=6, decimal_places=3, blank=True, null=True, help_text="Resolution (identical in both latitudinal and longitudinal direction; used for folder naming)")
	processing3_spatial_resolution_unit = models.CharField(max_length=15,choices=processed_spatial_resolution_unit_choices,default=None,blank=True,null=True, help_text="Unit of the spatial resolution (used for folder naming)")
	processing3_temporal_resolution = models.DecimalField(max_digits=3,decimal_places=1,blank=True,null=True, help_text="Temporal resolution (used for folder naming)")
	processing3_temporal_resolution_unit = models.CharField(max_length=10,choices=processed_temporal_resolution_unit_choices,default=None,blank=True,null=True, help_text="Unit of the temporal resolution (used for folder naming)")

	# non-editable
	processing_string = models.CharField(max_length=100, blank=False, null=False, default="original", editable=False, help_text="Auto-generated processing string that is used to name the sub-directories within the processed directory")
	path_to_data_directory_dataset = models.CharField(max_length=1000,editable=False,blank=True,null=True, help_text="Path to the physical location of the dataset realization (via dataset name)")
	path_to_data_directory_variable = models.CharField(max_length=1000,editable=False,blank=True,null=True, help_text="Path to the alternative location of the dataset realization (via variable name; this is not an actual folder but corresponds to a link to the respective dataset direcotry)")	
	creation_date = models.DateField(auto_now_add=True, editable=False)
	modification_date = models.DateField(auto_now=True, editable=False)

	class Meta:
		verbose_name = "processed dataset realization"
		verbose_name_plural = "processed dataset realizations"

	def clean(self):
		if str(self.dataset_realization) == "undefined": # TODO: this conditional test can be removed if this error is really fixed now.
			raise ValidationError("'undefined' DR found!")
		
		self.dataset_realization.subdataset.supdataset.full_clean()
		self.dataset_realization.subdataset.supdataset.save()
		self.dataset_realization.subdataset.full_clean()
		self.dataset_realization.subdataset.save()
		self.dataset_realization.full_clean()
		self.dataset_realization.save()

		if not self.data_processed and (self.processing1 or self.processing1_spatial_resolution or self.processing1_spatial_resolution_unit or self.processing1_temporal_resolution or self.processing1_temporal_resolution_unit or self.processing2 or self.processing2_spatial_resolution or self.processing2_spatial_resolution_unit or self.processing2_temporal_resolution or self.processing2_temporal_resolution_unit or self.processing3 or self.processing3_spatial_resolution or self.processing3_spatial_resolution_unit or self.processing3_temporal_resolution or self.processing3_temporal_resolution_unit):
			raise ValidationError("You chose that this processed dataset realization has not been post-processed but still chose one or several post-processing parameters.")

		if self.data_processed and ((self.processing2!="" or self.processing3!="") and (not self.processing1 or self.processing1=="")):
			raise ValidationError("When providing a processing identifier for processing step(s) 2 or 3, you also have to provide an identifier for processing step 1.")			

		if self.data_processed and (not self.processing1 or self.processing1==""):
			raise ValidationError("You chose that this processed dataset realization has been post-processed but forgot to choose which processing step(s) has(have) been applied. You have to provide at least the string identifying the first processing step (processing1)!")

		if self.processing1 != "regrid" and (self.processing1_spatial_resolution or self.processing1_spatial_resolution_unit):
			raise ValidationError("The chosen processing string for processing step 1 does not allow to choose a spatial resolution or spatial resolution unit.")
		if self.processing2 != "regrid" and (self.processing2_spatial_resolution or self.processing2_spatial_resolution_unit):
			raise ValidationError("The chosen processing string for processing step 2 does not allow to choose a spatial resolution or spatial resolution unit."+str(self.processing2_spatial_resolution)+","+str(self.processing2_spatial_resolution_unit))
		if self.processing3 != "regrid" and (self.processing3_spatial_resolution or self.processing3_spatial_resolution_unit):
			raise ValidationError("The chosen processing string for processing step 3 does not allow to choose a spatial resolution or spatial resolution unit.")

		if self.processing1 not in ["tmean","tmax","tmin","tsum"] and (self.processing1_temporal_resolution or self.processing1_temporal_resolution_unit):
			raise ValidationError("The chosen processing string for processing step 1 does not allow to choose a temporal resolution or temporal resolution unit.")
		if self.processing2 not in ["tmean","tmax","tmin","tsum"] and (self.processing2_temporal_resolution or self.processing2_temporal_resolution_unit):
			raise ValidationError("The chosen processing string for processing step 2 does not allow to choose a temporal resolution or temporal resolution unit.")
		if self.processing3 not in ["tmean","tmax","tmin","tsum"] and (self.processing3_temporal_resolution or self.processing3_temporal_resolution_unit):
			raise ValidationError("The chosen processing string for processing step 3 does not allow to choose a temporal resolution or temporal resolution unit.")		

		if self.processing1_temporal_resolution and (not self.processing1_temporal_resolution_unit or self.processing1_temporal_resolution_unit==""):
			raise ValidationError("You have to choose a temporal resolution unit of the applied processing step 1.")
		if self.processing2_temporal_resolution and (not self.processing2_temporal_resolution_unit or self.processing2_temporal_resolution_unit==""):
			raise ValidationError("You have to choose a temporal resolution unit of the applied processing step 2.")
		if self.processing3_temporal_resolution and (not self.processing3_temporal_resolution_unit or self.processing3_temporal_resolution_unit==""):
			raise ValidationError("You have to choose a temporal resolution unit of the applied processing step 3.")

		if self.processing1_spatial_resolution and (not self.processing1_spatial_resolution_unit or self.processing1_spatial_resolution_unit==""):
			raise ValidationError("You have to choose a spatial resolution unit of the applied processing step 1.")
		if self.processing2_spatial_resolution and (not self.processing2_spatial_resolution_unit or self.processing2_spatial_resolution_unit==""):
			raise ValidationError("You have to choose a spatial resolution unit of the applied processing step 2.")
		if self.processing3_spatial_resolution and (not self.processing3_spatial_resolution_unit or self.processing3_spatial_resolution_unit==""):
			raise ValidationError("You have to choose a spatial resolution unit of the applied processing step 3.")

		if self.processing1_temporal_resolution_unit and not self.processing1_temporal_resolution:
			raise ValidationError("When providing a temporal resolution unit, you also have to apply a temporal resolution for processing step 1.")
		if self.processing2_temporal_resolution_unit and not self.processing2_temporal_resolution:
			raise ValidationError("When providing a temporal resolution unit, you also have to apply a temporal resolution for processing step 2.")
		if self.processing3_temporal_resolution_unit and not self.processing3_temporal_resolution:
			raise ValidationError("When providing a temporal resolution unit, you also have to apply a temporal resolution for processing step 3.")

		if self.processing1_spatial_resolution_unit and not self.processing1_spatial_resolution:
			raise ValidationError("When providing a spatial resolution unit, you also have to apply a spatial resolution for processing step 1.")
		if self.processing2_spatial_resolution_unit and not self.processing2_spatial_resolution:
			raise ValidationError("When providing a spatial resolution unit, you also have to apply a spatial resolution for processing step 2.")
		if self.processing3_spatial_resolution_unit and not self.processing3_spatial_resolution:
			raise ValidationError("When providing a spatial resolution unit, you also have to apply a spatial resolution for processing step 3.")

		if not self.data_processed:
			self.processing_string = "original"
		else:
			if self.processing1_temporal_resolution:
				self.processing_string = self.processing1 +  str('{0:g}'.format(float(self.processing1_temporal_resolution))) + str(self.processing1_temporal_resolution_unit)
			elif self.processing1_spatial_resolution:
				self.processing_string = self.processing1 + str('{0:g}'.format(float(self.processing1_spatial_resolution))) + str(self.processing1_spatial_resolution_unit)
			else:
				self.processing_string = self.processing1
			if (self.processing2):
				if self.processing2_temporal_resolution:
					self.processing_string = self.processing_string + "_" + self.processing2 + str('{0:g}'.format(float(self.processing2_temporal_resolution))) + str(self.processing2_temporal_resolution_unit)
				elif self.processing2_spatial_resolution:
					self.processing_string = self.processing_string + "_" + self.processing2 + str('{0:g}'.format(float(self.processing2_spatial_resolution))) + str(self.processing2_spatial_resolution_unit)
				else:
					self.processing_string = self.processing_string + "_" + self.processing2
				if (self.processing3):
					if self.processing3_temporal_resolution:
						self.processing_string = self.processing_string + "_" + self.processing3 + str('{0:g}'.format(float(self.processing3_temporal_resolution))) + str(self.processing3_temporal_resolution_unit)
					elif self.processing3_spatial_resolution:
						self.processing_string = self.processing_string + "_" + self.processing3 + str('{0:g}'.format(float(self.processing3_spatial_resolution))) + str(self.processing3_spatial_resolution_unit)
					else:
						self.processing_string = self.processing_string + "_" + self.processing3
		
		data_root = "/net/exo/landclim/data/dataset"
		if not self.dataset_realization:
			raise ValidationError("DatasetRealization missing for PDR "+str(self))
			if not self.dataset_realization.subdataset:
				raise ValidationError("SubDataset missing for PDR "+str(self))
		elif str(self.dataset_realization) == "undefined":
			raise ValidationError("DatasetRealization missing for PDR "+str(self))

		data_path = os.path.join(data_root, self.dataset_realization.subdataset.supdataset.short_name.replace(' ','-'))
		if self.dataset_realization.subdataset.name != None and self.dataset_realization.subdataset.name != "":
			data_path = data_path + "_" + self.dataset_realization.subdataset.name.replace(' ','-')

		if (self.dataset_realization.subdataset.processing_level!=None and self.dataset_realization.subdataset.processing_level!=''):
			data_path = data_path + "_L" + self.dataset_realization.subdataset.processing_level.replace(' ','-')

		if (self.dataset_realization.subdataset.band!=None and self.dataset_realization.subdataset.band!=''):
			data_path = data_path + "_" + self.dataset_realization.subdataset.band.replace(' ','-') + "-BAND"

		if (self.dataset_realization.subdataset.orbit_direction!=None and self.dataset_realization.subdataset.orbit_direction!=''):
			data_path = data_path + "_" + self.dataset_realization.subdataset.orbit_direction.replace(' ','-')

		if self.dataset_realization.subdataset.version!=None and self.dataset_realization.subdataset.version!='':
			data_path = os.path.join(data_path, self.dataset_realization.subdataset.version.replace(' ','-').lower())
		else:
			data_path = os.path.join(data_path, str(self.dataset_realization.subdataset.download_date).replace('-',''))

		doc_path = os.path.join(data_path, "doc")
		if self.dataset_realization.subdataset.link_location:
			if self.dataset_realization.subdataset.link_location == "":
				self.dataset_realization.subdataset.link_location = None
				data_path_dr = None
		if not self.dataset_realization.subdataset.link_location:
			if (self.dataset_realization.is_model==True):
				data_path = os.path.join(data_path, "model")
			elif (self.dataset_realization.is_mixed==True):
				data_path = os.path.join(data_path, "mixed")
			else:
				if (self.dataset_realization.spatial_resolution_unit!=None and self.dataset_realization.spatial_resolution_unit!='' and len(self.dataset_realization.spatial_resolution_unit)>0):
					data_path = os.path.join(data_path, str('{0:g}'.format(float(self.dataset_realization.spatial_resolution_lat))))
					if (self.dataset_realization.spatial_resolution_lat != self.dataset_realization.spatial_resolution_lon):
						data_path = data_path + "x" + str('{0:g}'.format(float(self.dataset_realization.spatial_resolution_lon)))

					data_path = data_path + self.dataset_realization.spatial_resolution_unit.replace(' ','-').replace('°','deg').lower()
				elif self.dataset_realization.country_scale==True:
					data_path = os.path.join(data_path, "country-scale")
				elif self.dataset_realization.basin_scale==True:
					data_path = os.path.join(data_path, "basin-scale")
				elif self.dataset_realization.point_scale==True:
					data_path = os.path.join(data_path, "point-scale")

				data_path = data_path + "_" + self.dataset_realization.grid.name.replace(' ','-').lower() + "_"
				if (self.dataset_realization.temporal_resolution_unit!=None and self.dataset_realization.temporal_resolution_unit!=''):
					data_path = data_path + str('{0:g}'.format(float(self.dataset_realization.temporal_resolution))) + self.dataset_realization.temporal_resolution_unit.replace(' ','-').lower()
				elif self.dataset_realization.irregular==True:
					data_path = data_path + "irregular"
				elif self.dataset_realization.time_invariant==True:
					data_path = data_path + "time-invariant"
			
			if not self.data_processed:
				data_path = os.path.join(data_path,"original")
			else:
				data_path = os.path.join(data_path,"processed", self.processing_string.replace(' ','-').lower())
		
		data_path_previous = self.path_to_data_directory_dataset
		self.path_to_data_directory_dataset = data_path
		self.path_to_data_directory_variable = "/".join(data_path.split("/")[0:6]).replace("/dataset", "/variable/$VARIABLE")

		data_path_exists = os.path.exists(data_path.replace('/net/exo/landclim/data/dataset','/nfs/exo/landclim/dataset'))
		if not data_path_exists and not self.dataset_realization.subdataset.link_location: # create path to this processed dataset realization, if it does not already exist; or move contents of previous processed dataset realization folder to new folder location
			cmd = "mkdir -p " + os.path.join(data_path.replace('/net/exo/landclim/data/dataset','/nfs/exo/landclim/dataset'),'scripts')
			# print("Create: "+cmd)
			os.system(cmd)
			if data_path_previous and data_path_previous != data_path:
				# print(data_path+" exists: "+str(data_path_exists)+" | "+data_path_previous+" exists: "+str(os.path.exists(data_path_previous.replace('/net/exo/landclim/data/dataset','/nfs/exo/landclim/dataset'))))
				cmd = "mv " + os.path.join(data_path_previous.replace('/net/exo/landclim/data/dataset','/nfs/exo/landclim/dataset'),"*") + " " + data_path.replace('/net/exo/landclim/data/dataset','/nfs/exo/landclim/dataset')  # moves all regular files and sub-directories
				# print("Move: " + cmd)
				os.system(cmd)
				cmd = "mv " + os.path.join(data_path_previous.replace('/net/exo/landclim/data/dataset','/nfs/exo/landclim/dataset'),".[!.]*") + " " + data_path.replace('/net/exo/landclim/data/dataset','/nfs/exo/landclim/dataset') # moves all hidden files and sub-directories
				os.system(cmd)
				cmd = "rm -r " + data_path_previous.replace('/net/exo/landclim/data/dataset','/nfs/exo/landclim/dataset') # removes the now-empty deprecated sub-directory
				# print("Remove: " + cmd)
				os.system(cmd)
				data_path_split = data_path_previous.replace('/net/exo/landclim/data/dataset','/nfs/exo/landclim/dataset').split("/")
				data_path_test = "/".join(data_path_split[0:8]) # test if DatasetRealization directory tree is empty
				data_paths_test = [x[0] for x in os.walk(data_path_test)]
				n_data_paths_test = len(data_paths_test) - 1
				# print(data_path_test+": "+str(n_data_paths_test))
				if (n_data_paths_test == 1):
					cmd = "rm -r " + data_path_test
					# print("Remove: " + cmd)
					os.system(cmd)
					data_path_test = "/".join(data_path_split[0:7]) # test if SubDataset directory tree is empty
					data_paths_test = [x[0] for x in os.walk(data_path_test)]
					n_data_paths_test = len(data_paths_test)
					# print(data_path_test+": "+str(n_data_paths_test))
					if (n_data_paths_test == 1):
						cmd = "rm -r " + data_path_test
						# print("Remove: " + cmd)
						os.system(cmd)
						data_path_test = "/".join(data_path_split[0:6]) # test if SupDataset directory tree is empty
						data_paths_test = [x[0] for x in os.walk(data_path_test)]
						n_data_paths_test = len(data_paths_test)
						# print(data_path_test+": "+str(n_data_paths_test))
						if (n_data_paths_test == 1):
							cmd = "rm -r " + data_path_test
							# print("Remove: " + cmd)
							os.system(cmd)


		if self.dataset_realization.subdataset.primary_variable_groups.all() is not None:
			if len(self.dataset_realization.subdataset.primary_variable_groups.all()) > 0: # link the path to this processed dataset realization to its associated primary variable groups
				for pvar in self.dataset_realization.subdataset.primary_variable_groups.all():
					pvar = str(pvar).replace(" ","-").lower()
					pvar_path = self.path_to_data_directory_variable.replace("/$VARIABLE", "/"+pvar).replace('/net/exo/landclim/data/variable','/nfs/exo/landclim/variable')
					pvar_path_exists = os.path.exists(pvar_path)
					if not pvar_path_exists:
						cmd = "mkdir -p " + pvar_path
						os.system(cmd)

					link_exists = os.path.islink(os.path.join(pvar_path,data_path.split("/")[6]))
					if not link_exists:
						cmd = "cd " + pvar_path + "; ln -s " + "/".join(data_path.split("/")[0:7]).replace('/net/exo/landclim/data/dataset/','../../dataset/')
						os.system(cmd)

		self.dataset_realization_id = self.dataset_realization.pk

		self.key = slugify(str(self.dataset_realization_id)+"_"+str(self.processing_string))
		self.save()

	def __str__(self):
		return u'%s' % (self.processing_string)
