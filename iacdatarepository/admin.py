from .models import Grid, Organization, GeographicRegion, People, VariableGroup, SupDataset, SubDataset, DatasetRealization, ProcessedDatasetRealization
from django.contrib import admin
from django.forms import Select, TextInput, Textarea, ModelChoiceField, ChoiceField
import nested_admin


class OrganizationAdmin(admin.ModelAdmin):
    list_display = ('name', 'modification_date', 'creation_date')
    list_filter = ['creation_date']
    search_fields = ['name']
    def has_add_permission(self, request):
        return True


class PeopleAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Is group member',{'fields': ['is_staff']}),
        ('Name', {'fields': ['title','last_name','first_name','iac_user_name']}),
        ('Organzation', {'fields': ['organization']}),
        ('Address', {'fields': ['address']}),        
        ('E-Mail', {'fields': ['email']}),
        ('Phone',{'fields': ['telephone']}),
        ('Fax',{'fields': ['fax']}),
    ]
    search_fields = ['last_name','first_name']
    filter_horizontal = ('organization',)
    list_display = ('last_name','first_name','email','is_staff','creation_date')
    list_filter = ['creation_date']
    def has_add_permission(self, request):
        return True


class ProcessedDatasetRealizationInline(nested_admin.NestedStackedInline): # nesting level 3
    model = ProcessedDatasetRealization
    readonly_fields=('processing_string',)
    fieldsets = [
       ('Original or Processed', {'fields': ['data_processed','processing_string']}),
       ('General Processing Information', {'fields': [('processing_tool','processing_tool_version'),('processing_description')]}),
       ('Processing Step 1', {'fields': [('processing1'),('processing1_spatial_resolution','processing1_spatial_resolution_unit'),('processing1_temporal_resolution','processing1_temporal_resolution_unit')]}),
       ('Processing Step 2', {'fields': [('processing2'),('processing2_spatial_resolution','processing2_spatial_resolution_unit'),('processing2_temporal_resolution','processing2_temporal_resolution_unit')]}),
       ('Processing Step 3', {'fields': [('processing3'),('processing3_spatial_resolution','processing3_spatial_resolution_unit'),('processing3_temporal_resolution','processing3_temporal_resolution_unit')]}),
    ]
    extra = 0
    min_num = 1 # Provide at least one ProcessedDatasetRealization for each DatasetRealization
    inline_classes=("grp-collapse", "grp-open")
    classes=("grp-collapse", "grp-open")
    def has_add_permission(self, request):
        return True


class DatasetRealizationInline(nested_admin.NestedStackedInline): # nesting level 2
    model = DatasetRealization
    fieldsets = [
        ('Is mixed or model', {'fields': [('is_mixed','is_model')]}),
        ('Spatial Resolution', {'fields': [('spatial_resolution_lat','spatial_resolution_lon','spatial_resolution_unit'),('country_scale','point_scale','basin_scale'),'grid']}),
        ('Temporal Resolution', {'fields': [('temporal_resolution','temporal_resolution_unit'),('irregular','time_invariant')]}),
    ]
    inlines = [ProcessedDatasetRealizationInline]
    extra = 0
    min_num = 1 # Provide at least one DatasetRealization for each SubDataset
    inline_classes=("grp-collapse", "grp-open")
    classes=("grp-collapse", "grp-open")
    def has_add_permission(self, request):
        return True


class SubDatasetInline(nested_admin.NestedStackedInline): # nesting level 1
    model = SubDataset
    filter_horizontal = ('primary_variable_groups',)
    fieldsets = [
        ('Identifier', {'fields': [('name','download_date'),('version')]}),
        ('Link to External Data', {'fields': ['link_location']}),
        ('Satellite Data-Specific Information', {'fields': [('processing_level','band','orbit_direction')]}),
        ('Variable Groups', {'fields': [('primary_variable_groups')]}),
        ('Data Description and Documentation (valid for only this Sub-Dataset)', {'fields': [('summary'),('publication_doi','dataset_doi'),('project_url','data_source_url'),('known_issues','known_issues_link'),('data_policy','data_policy_link'),('manual_link','technical_manual_link','quick_user_guide_link'),('data_origin_category','source_dataset','source_processing')]}),
        ('Sample file (used to retrieve the information for all of the fields shown below)', {'fields': [('sample_file')]}),
        ('File format',{'fields':['file_format']}),
        ('Geographic Information', {'fields': [('geographic_region'),('bounding_longitude_west','bounding_longitude_east'),('bounding_latitude_south','bounding_latitude_north')]}),
        ('Time Range', {'fields': [('time_span_start','time_span_end')]}),
    ]
    inlines = [DatasetRealizationInline]
    extra = 0
    min_num = 1 # Provide at least one SubDataset for each SupDataset
    inline_classes=("grp-collapse", "grp-open")
    classes=("grp-collapse", "grp-open")
    def has_add_permission(self, request):
        return True


class SupDatasetAdmin(nested_admin.NestedModelAdmin): # nesting level 0 (=top level)
    model = SupDataset
    view_on_site = False
    list_display = ('short_name', 'full_name', 'data_origin_category', 'registration_required', 'special_restrictions', 'main_user', 'has_iac_read_access', 'is_linked_to_net_atmos', 'modification_date', 'creation_date')
    search_fields = ['short_name', 'full_name', 'data_origin_category']
    save_on_top = True
    save_as = True
    fieldsets = [
        ('Identifier', {'fields': [('short_name','full_name')]}),
        ('Contact Information', {'fields': [('organization','contact'),('main_user')]}),
        ('Permissions / Restrictions', {'fields': [('special_restrictions','registration_required')]}),
        ('Data Description and Documentation (valid for all Sub-Datasets)', {'fields': [('summary'),('publication_doi','dataset_doi'),('project_url','data_source_url'),('known_issues','known_issues_link'),('data_policy','data_policy_link'),('manual_link','technical_manual_link','quick_user_guide_link'),('data_origin_category','source_dataset','source_processing')]}),
        ('Fields required for datasets stored in the IAC data repository (/net/atmos/data)', {'fields': [('has_iac_read_access'),('is_linked_to_net_atmos'),('iac_readme_summary'),('iac_readme_contact'),('iac_readme_links')]}),
    ]
    inlines =  [SubDatasetInline]
    extra = 0
    min_num = 1
    inline_classes=("grp-collapse", "grp-open")
    classes=("grp-collapse", "grp-open")
    def has_add_permission(self, request):
        return True


admin.site.register(SupDataset, SupDatasetAdmin)
admin.site.register(GeographicRegion)
admin.site.register(Grid)
admin.site.register(Organization, OrganizationAdmin)
admin.site.register(People, PeopleAdmin)
admin.site.register(VariableGroup)
