import datetime
from .models import SupDataset, SubDataset, DatasetRealization, ProcessedDatasetRealization, People, Organization, VariableGroup, Grid
from .tables import SubDatasetTable, ProcessedDatasetRealizationTable
from django.shortcuts import get_object_or_404
from django.views import generic
from django_tables2 import SingleTableView
from django_tables2.export.views import ExportMixin


class SubDatasetSearchView(ExportMixin, SingleTableView):
    template_name = 'search'
    table_class = SubDatasetTable
    table_pagination = False
    model = SubDataset


class ProcessedDatasetRealizationSearchView(ExportMixin, SingleTableView):
    template_name = 'search'
    table_class = ProcessedDatasetRealizationTable
    table_pagination = False
    model = ProcessedDatasetRealization


class DatasetListView(generic.ListView):
    model = SupDataset
    context_object_name = 'dataset_list'
    slug_field = 'key'


class LatestDatasetListView(generic.ListView):
    model = SupDataset
    context_object_name = 'dataset_list'

    def get_queryset(self):
        last_month = datetime.datetime.today() - datetime.timedelta(days=30)
        return SupDataset.objects.filter(modification_date__gte=last_month).order_by('-modification_date')


class DatasetDetailView(generic.DetailView):
    model = SupDataset
    slug_field = 'key'
    context_object_name = 'dataset'

    def get_object(self):
        object = get_object_or_404(self.model, key=self.kwargs['key'])
        return object