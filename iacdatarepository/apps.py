from django.apps import AppConfig

class MyAppConfig(AppConfig):
    name = 'iacdatarepository'
    verbose_name = 'IAC Landclim Data Repository'

    def ready(self):
        import iacdatarepository.signals.handlers