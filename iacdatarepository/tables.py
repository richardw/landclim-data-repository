from django.utils.html import format_html
import django_tables2 as tables
from .models import ProcessedDatasetRealization, SubDataset
from slugify import slugify


class ProcessedDatasetRealizationTable(tables.Table):
	supdataset = tables.Column(accessor='dataset_realization.subdataset.supdataset', verbose_name="Sup-Dataset")
	subdataset = tables.Column(accessor='dataset_realization.subdataset', verbose_name="Sub-Dataset")
	path_to_data_directory_dataset = tables.Column(verbose_name="Path To Data Directory")
	primary_variable_groups = tables.Column(accessor='dataset_realization.subdataset.all_primary_variable_groups', verbose_name="Primary Variable Groups")	
	
	def render_supdataset(self, value):
		return format_html('<a href=../datasets/{}>{}</a>', slugify(str(value)), str(value))

	class Meta:
		model = ProcessedDatasetRealization
		fields = ("supdataset","subdataset","dataset_realization","path_to_data_directory_dataset","primary_variable_groups","processing_string","processing_performed_by","processing_tool","processing_tool_version","processing_description")
		orderable = False
		

class SubDatasetTable(tables.Table):
	supdataset_short_name = tables.Column(accessor='supdataset', verbose_name="Sup-Dataset Short Name")
	supdataset_full_name = tables.Column(accessor='supdataset.full_name', verbose_name="Sup-Dataset Full Name")
	organization = tables.Column(accessor='supdataset.organization', verbose_name="Organization")
	contact = tables.Column(accessor='supdataset.contact', verbose_name="Contact")
	main_user = tables.Column(accessor='supdataset.main_user', verbose_name="Main User")
	special_restrictions = tables.BooleanColumn(accessor='supdataset.special_restrictions', verbose_name="Special Restrictions")
	registration_required = tables.BooleanColumn(accessor='supdataset.registration_required', verbose_name="Registration Required")

	name = tables.Column(verbose_name="Sub-Dataset Name")
	all_primary_variable_groups = tables.Column(verbose_name="Primary Variable Groups")	
	download_date = tables.DateColumn()
	time_span_start = tables.DateColumn()
	time_span_end = tables.DateColumn()
	
	def render_supdataset_short_name(self, value):
		return format_html('<a href=../datasets/{}>{}</a>', slugify(str(value)), str(value))

	class Meta:
		model = SubDataset
		fields = ("supdataset_short_name","supdataset_full_name","name","version","download_date","all_primary_variable_groups","data_origin_category","time_span_start","time_span_end","geographic_region","organization","contact","main_user","special_restrictions","registration_required")
		orderable = False