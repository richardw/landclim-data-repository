import logging
import os
import subprocess
from django.core.exceptions import ValidationError
from django.db.models.signals import pre_delete, post_save
from django.dispatch import receiver
from iacdatarepository.models import Grid, Organization, GeographicRegion, People, VariableGroup, SupDataset, SubDataset, DatasetRealization, ProcessedDatasetRealization


logger = logging.getLogger(__name__)


@receiver(pre_delete, sender=SupDataset)
def pre_delete_sup(sender, instance, signal, using, **kwargs):
	logger.warning("running pre_delete_sup")
	sup = instance.subdataset_set.first()
	if sup:
		sup = sup.datasetrealization_set.first()
		if sup:
			sup = sup.processeddatasetrealization_set.first() # get an arbitrary processed dataset relization (the first) belonging to the current SupDataset

		'''
		Upon deletion of an entire SupDataset, we need to ensure that the corresponding data directory contains no files (excluding README files)
		'''
		path_del_dataset = sup.path_to_data_directory_dataset.replace("/net/exo/","/nfs/exo/").replace("/landclim/data/","/landclim/")
		path_del_dataset = "/".join(path_del_dataset.split("/")[0:6])

		cmd = "find "+path_del_dataset+" \( -not -type l -not -name \"README*\" -not -name \"scripts\" -not -name \"doc\" \) -type f"	
		out = subprocess.check_output(cmd,shell=True).decode("utf-8", "strict")
		if (len(out) > 0):
			raise ValidationError("The data directory of this SupDataset can not be deleted as it still contains files - you have to manually delete those files first: "+out.replace("\n"," "))
		else:
			del(out,cmd)
			if path_del_dataset.split("/")[-1] == "dataset" or path_del_dataset.split("/")[-1] == "variable": # ensure that nobody is ever trying to delete the entire directory tree; assumes that no supdataset is ever named 'dataset' or 'variable'
				raise ValidationError("Attempted to delete "+path_del_dataset+". Operation interrupted!")

			cmd = "rm -rf "+path_del_dataset		
			logger.warning("Executing delete command:"+cmd)
			os.system(cmd)

	# logger.warning("done pre_delete_sup")


@receiver(pre_delete, sender=ProcessedDatasetRealization)
def pre_delete_pdr(sender, instance, signal, using, **kwargs):
	logger.warning("running pre_delete_pdr")
	processeddatasetrealization_id = instance._get_pk_val()	
	if ProcessedDatasetRealization.objects.filter(pk=processeddatasetrealization_id):
		processeddatasetrealization_obj = ProcessedDatasetRealization.objects.get(pk=processeddatasetrealization_id)
		datasetrealization_obj = processeddatasetrealization_obj.dataset_realization

		if datasetrealization_obj:
			subdataset_obj = datasetrealization_obj.subdataset

			'''
			Upon deletion of a processed dataset realization, we need to check for obsolete dataset directories (excluding README files and scripts sub-directories)
			'''
			if instance.path_to_data_directory_dataset != None:
				path_del_dataset = instance.path_to_data_directory_dataset.replace("/net/exo/","/nfs/exo/").replace("/landclim/data/","/landclim/")
				if subdataset_obj:
					if (subdataset_obj.link_location != None and subdataset_obj.link_location != ''):
						nn = [n for n in range(len(path_del_dataset)) if path_del_dataset.find('/', n) == n][5] # position of 5th '/' character in old datset path
						path_del_dataset = path_del_dataset[0:nn]+"/"

				cmd = "find "+path_del_dataset+" \( -not -type l -not -name \"README*\" -not -name \"scripts\" -not -name \"doc\" \)"
				out = subprocess.check_output(cmd,shell=True).decode("utf-8", "strict")
				logger.warning('Paths 0 found: '+out.replace("\n",", ")+" | path_del_dataset = "+str(path_del_dataset))
				if (out != path_del_dataset+"\n"): # i.e., the directory contains files or subdirectories that need to be (re)moved first
					raise ValidationError("There are obsolte data directories that can not be deleted as they still contain files - you have to manually delete those files first: "+out.replace("\n"," "))
				else:
					del(out,cmd)
					cmd = "rm -rf "+path_del_dataset
					logger.warning("Executing delete command:"+cmd)
					os.system(cmd)
					parts = path_del_dataset.split("/")
					pseq = [i for i in range(6, len(parts))]
					pseq = pseq[::-1]
					logger.warning("Attempting to delete " + "/".join(parts[0:6]))

					for p in pseq: # backwards-recursively check if superior directories are also empty (down to directory level 6, which corresponds to the directory level of the sup-dataset); if a directory is empty, delete it
						path_temp = '/'.join(parts[0:p])
						cmd = "find "+path_temp+" \( -not -type l -not -name \"README*\" -not -name \"scripts\" -not -name \"doc\" \)"
						out = subprocess.check_output(cmd,shell=True).decode("utf-8", "strict")
						logger.warning('Paths '+str(p)+' found: '+out.replace("\n"," "))
						if (out == path_temp+"\n"):
							del(out,cmd)
							cmd = "rm -rf "+path_temp
							logger.warning("Deleting "+path_temp)
							os.system(cmd)
						else:
							del(out,cmd)
							break # break iteration as soon as any data files or directories are found

			# logger.warning("done pre_delete_pdr")