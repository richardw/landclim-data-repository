from . import views
from django.urls import path

urlpatterns = [
    path('', views.LatestDatasetListView.as_view(template_name="iacdatarepository/latest.html"), name='home'),
    path('datasets/', views.DatasetListView.as_view(template_name="iacdatarepository/datasets.html"), name='datasets'),
    path('datasets/<slug:key>/', views.DatasetDetailView.as_view(template_name="iacdatarepository/detail.html"), name='dataset-detail'),
    path('search/', views.SubDatasetSearchView.as_view(template_name="iacdatarepository/search.html"), name='search'),
    path('search_processed/', views.ProcessedDatasetRealizationSearchView.as_view(template_name="iacdatarepository/search.html"), name='search_processed'),
]