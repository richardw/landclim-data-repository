# Generated by Django 2.2.1 on 2019-05-24 16:28

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('iacdatarepository', '0010_auto_20190524_1625'),
    ]

    operations = [
        migrations.AlterField(
            model_name='supdataset',
            name='contact',
            field=models.ForeignKey(blank=True, help_text='Main dataset contact person', limit_choices_to={'is_staff': False}, null=True, on_delete=django.db.models.deletion.SET_NULL, to='iacdatarepository.People'),
        ),
        migrations.AlterField(
            model_name='supdataset',
            name='main_user',
            field=models.ForeignKey(blank=True, default=75, help_text='Staff member that is the main user of the sub-dataset or that has the highest experience with the sub-dataset', limit_choices_to={'is_staff': True}, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='supdataset_user', to='iacdatarepository.People'),
        ),
    ]
