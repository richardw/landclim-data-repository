#!/webapps/landclimdata/.pyenv/shims/python

import django
import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "landclim_data_repository.settings")
django.setup()

from django.core.exceptions import ValidationError
from iacdatarepository.models import Grid, Organization, GeographicRegion, People, VariableGroup, SupDataset, SubDataset, DatasetRealization, ProcessedDatasetRealization

os.system("rm -r /nfs/exo/landclim/variable/*")
pdrs = ProcessedDatasetRealization.objects.all()
for pdr in pdrs:
	if pdr.dataset_realization.subdataset.primary_variable_groups.all() is not None:
		if len(pdr.dataset_realization.subdataset.primary_variable_groups.all()) > 0: # link the path to this processed dataset realization to its associated primary variable groups
			for pvar in pdr.dataset_realization.subdataset.primary_variable_groups.all():
				pvar = str(pvar).replace(" ","-").lower()
				data_path = pdr.path_to_data_directory_dataset
				pvar_path = pdr.path_to_data_directory_variable.replace("/$VARIABLE", "/"+pvar).replace('/net/exo/landclim/data/variable','/nfs/exo/landclim/variable')
				pvar_path_exists = os.path.exists(pvar_path)
				if not pvar_path_exists:
					cmd = "mkdir -p " + pvar_path
					os.system(cmd)
				link_exists = os.path.islink(os.path.join(pvar_path,data_path.split("/")[6]))
				if not link_exists:
					cmd = "cd " + pvar_path + "; ln -s " + "/".join(data_path.split("/")[0:7]).replace('/net/exo/landclim/data/dataset/','../../dataset/')
					os.system(cmd)
