#!/webapps/landclimdata/.pyenv/shims/python
# -*- coding: utf-8 -*-

import django
import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "landclim_data_repository.settings")
django.setup()

import datetime
import logging
import subprocess
from iacdatarepository.models import Grid, Organization, GeographicRegion, People, VariableGroup, SupDataset, SubDataset, DatasetRealization, ProcessedDatasetRealization
import re
from slugify import slugify

def exists(obj):
	if type(obj)==type(True):
		obj = True
	else:
		if obj:
			if len(str(obj)) == 0:
				obj = False
			else:
				obj = True
		else:
			obj = False
	return obj

def slugifyParagraphs(text):
	text = re.sub(
		' +',
		' ',
		text
	)
	text.replace("\n ","\n").replace("\n- ","\n* ").replace('"',"'")
	text = re.sub(
		'--+',
		'-----',
		text
	)
	text = re.sub(
		'\s$',
		'',
		text
	)
	return text

sups_to_process = []
pdrs = ProcessedDatasetRealization.objects.all()
for pdr in pdrs:
	if pdr.modification_date >= (datetime.date.today() - datetime.timedelta(days = 1)):
			sups_to_process.append(pdr.dataset_realization.subdataset.supdataset_id)

sups_to_process.sort()
sups_to_process = list(set(sups_to_process))

# sup_to_process = sups_to_process[0]
for sup_to_process in sups_to_process:
	sup_to_process = SupDataset.objects.get(pk=sup_to_process)
	subs_to_process = sup_to_process.subdataset_set.all()
	# sub_to_process = subs_to_process[0]
	for sub_to_process in subs_to_process:
		pdr = sub_to_process.datasetrealization_set.first().processeddatasetrealization_set.first()
		readme_dir = sub_to_process.data_root.replace("/net/exo","/nfs/exo").replace("/landclim/data","/landclim")
		doc_dir = os.path.join(readme_dir,"doc")
		doc_dir_external = doc_dir.replace("/nfs/exo","/net/exo").replace("/landclim","/landclim/data")
		readme_filename = sub_to_process.readme_file.replace("/net/exo","/nfs/exo").replace("/landclim/data","/landclim").replace(".html",".adoc")
		readme_lines = ""
		readme_lines += "= " + sup_to_process.short_name + " (" + sup_to_process.full_name + ")"
		readme_lines += "\nLast Change: "+str(datetime.date.today())
		readme_lines += "\n\r\n"
		if sup_to_process.registration_required:
			readme_lines += "WARNING: You have to register on the website of the data provider prior to using this dataset for publishing papers!" + "\n\r\n"
		if sup_to_process.special_restrictions:
			readme_lines += "IMPORTANT: Access to this dataset is restricted. Please get in touch with " + str(sup_to_process.main_user) + ".\n\r\n"
		readme_lines += "== "
		if not(exists(sub_to_process.name)) and not(exists(sub_to_process.version)):
			readme_lines += "Downloaded on "+ str(sub_to_process.download_date)
		else:
			if exists(sub_to_process.name) and exists(sub_to_process.version):
				readme_lines += sub_to_process.name+" "+sub_to_process.version
			elif exists(sub_to_process.version):
				readme_lines += sub_to_process.version
			elif exists(sub_to_process.name):
				readme_lines += sub_to_process.name
			if exists(sub_to_process.download_date):
				readme_lines += " (downloaded on " + str(sub_to_process.download_date)+")"
		if exists(sub_to_process.processing_level) or exists(sub_to_process.band) or exists(sub_to_process.orbit_direction):
			if exists(sub_to_process.processing_level):
				readme_lines += " Level "+sub_to_process.processing_level+" Product"
				if exists(sub_to_process.band) or exists(sub_to_process.orbit_direction):
					readme_lines += " |"
			if exists(sub_to_process.band):
				readme_lines += " Band " + sub_to_process.band
				if exists(sub_to_process.orbit_direction):
					readme_lines += " |"
			if exists(sub_to_process.orbit_direction):
				readme_lines += " Orbit Direction " + sub_to_process.orbit_direction
		readme_lines += "\n\r\n"
		if exists(sup_to_process.organization) or exists(sup_to_process.contact) or exists(sup_to_process.main_user):
			readme_lines += "=== Contact Information" + "\n\r\n"
			if exists(sup_to_process.organization):
				readme_lines += "*Organization* \n\r\n" + "++++" + str(sup_to_process.organization) + "\n\r\n"
			if exists(sup_to_process.contact):
				readme_lines += "*Remote Contact Person* \n\r\n" + "++++" + str(sup_to_process.contact) + "\n\r\n"
			if exists(sup_to_process.main_user):
				readme_lines += "*Main User @ IAC-Landclim* \n\r\n" + "++++" + str(sup_to_process.main_user) + "\n\r\n"
		readme_lines += "=== Documentation and Links" + "\n\r\n"
		if exists(sup_to_process.summary):
			sup_to_process.summary = slugifyParagraphs(sup_to_process.summary)
			readme_lines += "*Summary / Abstract* \n\r\n" + "++++" + sup_to_process.summary + "\n\r\n"
		if exists(sub_to_process.summary) and sup_to_process.summary != sub_to_process.summary:
			sub_to_process.summary = slugifyParagraphs(sub_to_process.summary)
			readme_lines += "*Add. Summary / Abstract* \n\r\n" + "++++" + sub_to_process.summary + "\n\r\n"
		if exists(sup_to_process.publication_doi):
			readme_lines += "*Publication DOI(s)* \n\r\n" + "++++" + sup_to_process.publication_doi + "\n\r\n"
		if exists(sub_to_process.publication_doi) and sup_to_process.publication_doi != sub_to_process.publication_doi:
			readme_lines += "*Add. Publication DOI(s)* \n\r\n" + "++++" + sub_to_process.publication_doi + "\n\r\n"
		if exists(sup_to_process.dataset_doi):
			readme_lines += "*Dataset DOI(s)* \n\r\n" + "++++" + sup_to_process.dataset_doi + "\n\r\n"
		if exists(sub_to_process.dataset_doi) and sup_to_process.dataset_doi != sub_to_process.dataset_doi:
			readme_lines += "*Add. Dataset DOI(s)* \n\r\n" + "++++" + sub_to_process.dataset_doi + "\n\r\n"
		if exists(sup_to_process.project_url):
			readme_lines += "*Project URL* \n\r\n" + sup_to_process.project_url + "\n\r\n"
		if exists(sub_to_process.project_url) and sup_to_process.project_url != sub_to_process.project_url:
			readme_lines += "*Add. Project URL* \n\r\n" + sub_to_process.project_url + "\n\r\n"
		if exists(sup_to_process.data_source_url):
			readme_lines += "*Data Source URL* \n\r\n" + sup_to_process.data_source_url + "\n\r\n"
		if exists(sub_to_process.data_source_url) and sup_to_process.data_source_url != sub_to_process.data_source_url:
			readme_lines += "*Add. Data Source URL* \n\r\n" + sub_to_process.data_source_url + "\n\r\n"
		if exists(sup_to_process.known_issues):
			sup_to_process.known_issues = slugifyParagraphs(sup_to_process.known_issues)
			readme_lines += "*Known Issues* \n\r\n" + "++++" + sup_to_process.known_issues + "\n\r\n"
		if exists(sub_to_process.known_issues) and sup_to_process.known_issues != sub_to_process.known_issues:
			sub_to_process.known_issues = slugifyParagraphs(sub_to_process.known_issues)
			readme_lines += "*Add. Known Issues* \n\r\n" + "++++" + sub_to_process.known_issues + "\n\r\n"
		if exists(sup_to_process.known_issues_link):
			readme_lines += "*Known Issues Document* \n\r\n" + "++++" + os.path.join(doc_dir_external,sup_to_process.known_issues_link) + "\n\r\n"
		if exists(sub_to_process.known_issues_link) and sup_to_process.known_issues_link != sub_to_process.known_issues_link:
			readme_lines += "*Add. Known Issues Document* \n\r\n" + "++++" + os.path.join(doc_dir_external,sub_to_process.known_issues_link) + "\n\r\n"
		if exists(sup_to_process.data_policy):
			sup_to_process.data_policy = slugifyParagraphs(sup_to_process.data_policy)
			readme_lines += "*Data Policy* \n\r\n" + "++++" + sup_to_process.data_policy + "\n\r\n"
		if exists(sub_to_process.data_policy) and sup_to_process.data_policy != sub_to_process.data_policy:
			sub_to_process.data_policy = slugifyParagraphs(sub_to_process.data_policy)
			readme_lines += "*Add. Data Policy* \n\r\n" + "++++" + sub_to_process.data_policy + "\n\r\n"
		if exists(sup_to_process.data_policy_link):
			readme_lines += "*Data Policy Document* \n\r\n" + "++++" + os.path.join(doc_dir_external,sup_to_process.data_policy_link) + "\n\r\n"
		if exists(sub_to_process.data_policy_link) and sup_to_process.data_policy_link != sub_to_process.data_policy_link:
			readme_lines += "*Add. Data Policy Document* \n\r\n" + "++++" + os.path.join(doc_dir_external,sub_to_process.data_policy_link) + "\n\r\n"
		if exists(sup_to_process.manual_link):
			readme_lines += "*Manual Document* \n\r\n" + "++++" + os.path.join(doc_dir_external,sup_to_process.manual_link) + "\n\r\n"
		if exists(sub_to_process.manual_link) and sup_to_process.manual_link != sub_to_process.manual_link:
			readme_lines += "*Add. Manual Document* \n\r\n" + "++++" + os.path.join(doc_dir_external,sub_to_process.manual_link) + "\n\r\n"
		if exists(sup_to_process.technical_manual_link):
			readme_lines += "*Technical Manual Document* \n\r\n" + "++++" + os.path.join(doc_dir_external,sup_to_process.technical_manual_link) + "\n\r\n"
		if exists(sub_to_process.technical_manual_link) and sup_to_process.technical_manual_link != sub_to_process.technical_manual_link:
			readme_lines += "*Add. Technical Manual Document* \n\r\n" + "++++" + os.path.join(doc_dir_external,sub_to_process.technical_manual_link) + "\n\r\n"
		if exists(sup_to_process.quick_user_guide_link):
			readme_lines += "*Quick User Guide Document* \n\r\n" + "++++" + os.path.join(doc_dir_external,sup_to_process.quick_user_guide_link)+ "\n\r\n"
		if exists(sub_to_process.quick_user_guide_link) and sup_to_process.quick_user_guide_link != sub_to_process.quick_user_guide_link:
			readme_lines += "*Add. Quick User Guide Document* \n\r\n" + "++++" + os.path.join(doc_dir_external,sub_to_process.quick_user_guide_link) + "\n\r\n"
		if exists(sup_to_process.data_origin_category) or exists(sup_to_process.source_dataset) or exists(sup_to_process.source_processing) or exists(sub_to_process.data_origin_category) or exists(sub_to_process.source_dataset) or exists(sub_to_process.source_processing):
			readme_lines += "=== Data Category and Data Origin" + "\n\r\n"
			if exists(sup_to_process.data_origin_category):
				readme_lines += "*Data Category* \n\r\n" + "++++" + sup_to_process.data_origin_category + "\n\r\n"
			if exists(sub_to_process.data_origin_category) and sup_to_process.data_origin_category != sub_to_process.data_origin_category:
				readme_lines += "*Add. Data Category* \n\r\n" + "++++" + sub_to_process.data_origin_category + "\n\r\n"
			if exists(sup_to_process.source_dataset):
				readme_lines += "*Data Source* \n\r\n" + "++++" + sup_to_process.source_dataset + "\n\r\n"
			if exists(sub_to_process.source_dataset) and sup_to_process.source_dataset != sub_to_process.source_dataset:
				readme_lines += "*Add. Data Source* \n\r\n" + "++++" + sub_to_process.source_dataset + "\n\r\n"
			if exists(sup_to_process.source_processing):
				readme_lines += "*Processing Applied to Data Source* \n\r\n" + "++++" + sup_to_process.source_processing + "\n\r\n"
			if exists(sub_to_process.source_processing) and sup_to_process.source_processing != sub_to_process.source_processing:
				readme_lines += "*Add. Processing Applied to Data Source* \n\r\n" + "++++" + sub_to_process.source_processing + "\n\r\n"
		if exists(sub_to_process.primary_variable_groups.all()):
			readme_lines += "=== Primary Variables" + "\n\r\n" + "++++"
			readme_lines += sub_to_process.all_primary_variable_groups + "\n\r\n"
		if exists(sub_to_process.geographic_region) or exists(sub_to_process.bounding_latitude_north) or exists(sub_to_process.time_span_start):
			readme_lines += "=== Spatiotemporal Domain" + "\n\r\n"
			if exists(sub_to_process.geographic_region):
				readme_lines += "*Geographic Region* \n\r\n" + "++++" + str(sub_to_process.geographic_region)+"\n\r\n"
			if exists(sub_to_process.bounding_latitude_north):
				if sub_to_process.bounding_latitude_north > 0:
					hemisphere_north = "N"
				else:
					hemisphere_north = "S"
				if sub_to_process.bounding_latitude_south > 0:
					hemisphere_south = "N"
				else:
					hemisphere_south = "S"
				if sub_to_process.bounding_longitude_east > 0:
					hemisphere_east = "E"
				else:
					hemisphere_east = "W"
				if sub_to_process.bounding_longitude_west > 0:
					hemisphere_west = "E"
				else:
					hemisphere_west = "W"
				readme_lines += "*Spatial Bounds* \n\r\n" + "(" + str(abs(round(sub_to_process.bounding_latitude_north,0)))+"°"+hemisphere_north+", "+str(abs(round(sub_to_process.bounding_longitude_west,0)))+"°"+hemisphere_west+") , ("+str(abs(round(sub_to_process.bounding_latitude_south,0)))+"°"+hemisphere_south+", "+str(abs(round(sub_to_process.bounding_longitude_east,0)))+"°"+hemisphere_east+")"+"\n\r\n"
			if exists(sub_to_process.time_span_start):
				readme_lines += "*Time Span* \n\r\n" + "++++" + str(sub_to_process.time_span_start) + " - " + str(sub_to_process.time_span_end) + "\n\r\n"
		readme_lines += "\n\r\n"
		readme_lines += "=== Dataset Realizations\n|===\n|Realization |Post-Processed Version(s)\n\r\n"
		drs = sub_to_process.datasetrealization_set.all()
		for dr in drs:
			pdrs = dr.processeddatasetrealization_set.all()
			pdrs = ", ".join([str(p) for p in pdrs if str(p) != "original"])
			readme_lines += "|"+str(dr).replace("deg","°")+"\n|"+pdrs.replace("deg","°")+"\n\r\n"
			if dr != drs[len(drs)-1]:
				readme_lines += "\n\r\n"
		readme_lines += "|===\n\r\n"
		with open(readme_filename, 'w') as file:
		    file.write(readme_lines)
		print("Processing "+readme_filename)
		os.system("asciidoctor "+readme_filename)
		os.system("asciidoctor-pdf.ruby2.1 "+readme_filename)
