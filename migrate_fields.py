from collections import Counter
from decimal import Decimal
from django.core.exceptions import ValidationError
from iacdatarepository.models import  Grid, VariableGroup, SupDataset, SubDataset, DatasetRealization, ProcessedDatasetRealization
from slugify import slugify
import numpy as np
import re
import os
import subprocess

# sups = SupDataset.objects.all()
# sup = sups[0]
# sub_fields = ["main_user","special_restrictions","registration_required","data_type","number_of_dimensions","summary","publication_doi","dataset_doi","project_url","data_source_url","known_issues","known_issues_link","data_policy","data_policy_link","manual_link","quick_user_guide_link","technical_manual_link","data_origin_category","source_dataset","source_processing"]
# sub_field = sub_fields[0]

# for sup in sups:
# 	print("Processing "+str(sup))
# 	subs = SubDataset.objects.filter(supdataset=sup)
# 	sub = subs[0]
# 	vals_keep = {}
# 	for sub_field in sub_fields:
# 		vals_avail = []
# 		for sub in subs:
# 			vals_avail.append(getattr(sub,sub_field))
# 		if type(vals_avail[0]) is bool:
# 			if (any(vals_avail)):
# 				ind_keep = vals_avail.index(True)
# 			else:
# 				ind_keep = 0
# 		elif type(vals_avail[0]) is int:
# 			ind_keep = vals_avail.index(max(x for x in vals_avail if x is not None))
# 		elif any(['models.People' in str(type(v)) for v in vals_avail]):
# 			ind_keep = vals_avail.index([x for x in vals_avail if x is not None][0])
# 		elif type(vals_avail[0]) is str:
# 			strlens = [len(x) for x in vals_avail if x is not None]
# 			ind_keep = strlens.index(max(strlens))
# 		vals_keep[sub_field]=vals_avail[ind_keep]
# 	sup.main_user = vals_keep["main_user"]
# 	sup.special_restrictions = vals_keep["special_restrictions"]
# 	sup.registration_required = vals_keep["registration_required"]
# 	sup.data_type = vals_keep["data_type"]
# 	sup.number_of_dimensions = vals_keep["number_of_dimensions"]
# 	sup.summary = vals_keep["summary"]
# 	sup.publication_doi = vals_keep["publication_doi"]
# 	sup.dataset_doi = vals_keep["dataset_doi"]
# 	sup.project_url = vals_keep["project_url"]
# 	sup.data_source_url = vals_keep["data_source_url"]
# 	sup.known_issues = vals_keep["known_issues"]
# 	sup.known_issues_link = vals_keep["known_issues_link"]
# 	sup.data_policy = vals_keep["data_policy"]
# 	sup.data_policy_link = vals_keep["data_policy_link"]
# 	sup.manual_link = vals_keep["manual_link"]
# 	sup.quick_user_guide_link = vals_keep["quick_user_guide_link"]
# 	sup.technical_manual_link = vals_keep["technical_manual_link"]
# 	sup.data_origin_category = vals_keep["data_origin_category"]
# 	sup.source_dataset = vals_keep["source_dataset"]
# 	sup.source_processing = vals_keep["source_processing"]
# 	sup.save()


# subs = SubDataset.objects.all()
# sub = subs[0]
# dr_fields = ['time_span_continuous_available','time_span_start','time_span_end','primary_variable_groups','secondary_variable_groups']
# dr_field = dr_fields[0]

# for sub in subs:
# 	print("Processing "+str(sub.supdataset)+str(sub))
# 	drs = DatasetRealization.objects.filter(subdataset=sub)
# 	dr = drs[0]
# 	vals_keep = {}
# 	for dr_field in dr_fields:
# 		vals_avail = []
# 		for dr in drs:
# 			vals_avail.append(getattr(dr,dr_field))
# 		if type(vals_avail[0]) is bool:
# 			if (any(vals_avail)):
# 				ind_keep = vals_avail.index(True)
# 			else:
# 				ind_keep = 0
# 		elif any(['datetime' in str(type(v)) for v in vals_avail]):
# 			if 'start' in dr_field:
# 				ind_keep = vals_avail.index(min(x for x in vals_avail if x is not None))
# 			elif 'end' in dr_field:
# 				ind_keep = vals_avail.index(max(x for x in vals_avail if x is not None))
# 		elif any(['ManyRelatedManager' in str(type(v)) for v in vals_avail]):
# 			ind_keep = np.argmax([len(x.all()) for x in vals_avail]) # keep the M:1 relation that links to the maximum number of items
# 		elif type(vals_avail[0]) is str:
# 			strlens = [len(x) for x in vals_avail]
# 			ind_keep = strlens.index(max(strlens))
# 		vals_keep[dr_field]=vals_avail[ind_keep]
# 	sub.primary_variable_groups.set(vals_keep["primary_variable_groups"].all())
# 	sub.secondary_variable_groups.set(vals_keep["secondary_variable_groups"].all())
# 	sub.time_span_continuous_available = vals_keep["time_span_continuous_available"]
# 	sub.time_span_start = vals_keep["time_span_start"]
# 	sub.time_span_end = vals_keep["time_span_end"]
# 	sub.save()

# # create keys for all DatasetRealization objects in order to ensure (after everything is migrated) uniqueness of a dataset realization
# drs = DatasetRealization.objects.all()
# inc=0
# dr = drs[inc]
# for dr in drs:
# 	if not dr.subdataset:
# 		SubDataset.objects.create(pk=dr.subdataset_id,name="TBD",version="v0",supdataset=SupDataset.objects.get(pk=1)) # create dummy SubDataset if it does not exist (for whatever reason)
# 	dr.key = slugify(str(dr.subdataset.supdataset)+"_"+str(dr.subdataset)+"_"+str(dr.basin_scale)+"_"+str(dr.country_scale)+"_"+str(dr.grid)+"_"+str(dr.irregular)+"_"+str(dr.is_model)+"_"+str(dr.is_mixed)+"_"+str(dr.point_scale)+"_"+str(dr.spatial_resolution_lat)+"_"+str(dr.spatial_resolution_lon)+"_"+str(dr.spatial_resolution_unit)+"_"+str(dr.temporal_resolution)+"_"+str(dr.temporal_resolution_unit)+"_"+str(dr.time_invariant))
# 	dr.save()
# 	inc = inc + 1


# # ensure validity of DatasetRealizations
# drs = DatasetRealization.objects.all()
# for dr in drs:
# 	print("Processing "+str(dr))
# 	if dr.spatial_resolution_unit == "°":
# 		dr.spatial_resolution_unit = "deg"
# 	elif dr.spatial_resolution_unit == "arc-seconds":
# 		dr.spatial_resolution_unit = "sec"
# 	elif dr.spatial_resolution_unit == "arc-minutes":
# 		dr.spatial_resolution_unit = "min"
# 	if not dr.grid and not (dr.is_model or dr.is_mixed):
# 		dr.grid = Grid.objects.get(name="none")
# 	dr.full_clean()
# 	dr.save()

# ProcessedDatasetRealization.objects.all().delete()
# drs = DatasetRealization.objects.all()
# inc = 0
# dr = drs[inc]
# for dr in drs[inc:len(drs)]:
# 	print("Processing "+str(dr.subdataset.supdataset)+" | "+str(dr.subdataset)+" | "+str(dr))
# 	pdr = ProcessedDatasetRealization.objects.create(dataset_realization=dr)
# 	pdr.data_processed = dr.data_processed
# 	pdr.processing_string_original = dr.processing_string
# 	pdr.processing_performed_by = dr.processing_performed_by
# 	pdr.processing_tool = dr.processing_tool_version
# 	pdr.processing_description = dr.processing_description
# 	temp = pdr.processing_tool.split(" ")
# 	if len(temp)>1:
# 		pdr.processing_tool_version = temp[(len(temp)-1)]
# 		pdr.processing_tool = " ".join(temp[0:(len(temp)-1)])
# 	temp = pdr.processing_string_original.split("_")
# 	if len(temp) == 1:
# 		pdr.processing2 = None
# 		pdr.processing2_spatial_resolution = None
# 		pdr.processing2_spatial_resolution_unit = None
# 		pdr.processing2_temporal_resolution = None
# 		pdr.processing2_temporal_resolution_unit = None
# 		pdr.processing3 = None
# 		pdr.processing3_spatial_resolution = None
# 		pdr.processing3_spatial_resolution_unit = None
# 		pdr.processing3_temporal_resolution = None
# 		pdr.processing3_temporal_resolution_unit = None
# 	elif len(temp) == 2:
# 		pdr.processing3 = None
# 		pdr.processing3_spatial_resolution = None
# 		pdr.processing3_spatial_resolution_unit = None
# 		pdr.processing3_temporal_resolution = None
# 		pdr.processing3_temporal_resolution_unit = None
# 	if temp[0][0:6]=="regrid":
# 		pdr.processing1 = temp[0][0:6]
# 		if temp[0][6:99] != "":
# 			pdr.processing1_spatial_resolution = Decimal(re.sub(r"[A-Za-z]+", '', temp[0][6:99]))
# 			pdr.processing1_spatial_resolution_unit = re.sub(r"[^A-Za-z]+", '', temp[0][6:99])
# 		pdr.processing1_temporal_resolution = None
# 		pdr.processing1_temporal_resolution_unit = None
# 	elif temp[0][0:5]=="tmean":
# 		pdr.processing1 = temp[0][0:5]
# 		pdr.processing1_temporal_resolution = Decimal(re.sub(r"[A-Za-z]+", '', temp[0][5:99]))
# 		pdr.processing1_temporal_resolution_unit = re.sub(r"[^A-Za-z]+", '', temp[0][5:99])
# 		pdr.processing1_spatial_resolution = None
# 		pdr.processing1_spatial_resolution_unit = None
# 	elif temp[0][0:4] in ["tmin","tmax","tsum"]:
# 		pdr.processing1 = temp[0][0:4]
# 		pdr.processing1_temporal_resolution = Decimal(re.sub(r"[A-Za-z]+", '', temp[0][4:99]))
# 		pdr.processing1_temporal_resolution_unit = re.sub(r"[^A-Za-z]+", '', temp[0][4:99])
# 		pdr.processing1_spatial_resolution = None
# 		pdr.processing1_spatial_resolution_unit = None
# 	else:
# 		pdr.processing1 = temp[0]
# 		pdr.processing1_temporal_resolution = None
# 		pdr.processing1_temporal_resolution_unit = None
# 		pdr.processing1_spatial_resolution = None
# 		pdr.processing1_spatial_resolution_unit = None
# 	if len(temp) > 1:
# 		if temp[1][0:6]=="regrid":
# 			pdr.processing2 = temp[1][0:6]
# 			if temp[1][6:99] != "":
# 				pdr.processing2_spatial_resolution = Decimal(re.sub(r"[A-Za-z]+", '', temp[1][6:99]))
# 				pdr.processing2_spatial_resolution_unit = re.sub(r"[^A-Za-z]+", '', temp[1][6:99])
# 			pdr.processing2_temporal_resolution = None
# 			pdr.processing2_temporal_resolution_unit = None
# 		elif temp[1][0:5]=="tmean":
# 			pdr.processing2 = temp[1][0:5]			
# 			pdr.processing2_temporal_resolution = Decimal(re.sub(r"[A-Za-z]+", '', temp[1][5:99]))
# 			pdr.processing2_temporal_resolution_unit = re.sub(r"[^A-Za-z]+", '', temp[1][5:99])
# 			pdr.processing2_spatial_resolution = None
# 			pdr.processing2_spatial_resolution_unit = None
# 		elif temp[1][0:4] in ["tmin","tmax","tsum"]:
# 			pdr.processing2 = temp[1][0:4]
# 			pdr.processing2_temporal_resolution = Decimal(re.sub(r"[A-Za-z]+", '', temp[1][4:99]))
# 			pdr.processing2_temporal_resolution_unit = re.sub(r"[^A-Za-z]+", '', temp[1][4:99])
# 			pdr.processing2_spatial_resolution = None
# 			pdr.processing2_spatial_resolution_unit = None
# 		else:
# 			pdr.processing2 = temp[1]
# 			pdr.processing2_temporal_resolution = None
# 			pdr.processing2_temporal_resolution_unit = None
# 			pdr.processing2_spatial_resolution = None
# 			pdr.processing2_spatial_resolution_unit = None
# 	if len(temp) > 2:
# 		if temp[2][0:6]=="regrid":
# 			pdr.processing3 = temp[2][0:6]
# 			if temp[2][6:99] != "":
# 				pdr.processing3_spatial_resolution = Decimal(re.sub(r"[A-Za-z]+", '', temp[2][6:99]))
# 				pdr.processing3_spatial_resolution_unit = re.sub(r"[^A-Za-z]+", '', temp[2][6:99])
# 			pdr.processing3_temporal_resolution = None
# 			pdr.processing3_temporal_resolution_unit = None
# 		elif temp[2][0:5]=="tmean":
# 			pdr.processing3 = temp[2][0:5]
# 			pdr.processing3_temporal_resolution = Decimal(re.sub(r"[A-Za-z]+", '', temp[2][5:99]))
# 			pdr.processing3_temporal_resolution_unit = re.sub(r"[^A-Za-z]+", '', temp[2][5:99])
# 			pdr.processing3_spatial_resolution = None
# 			pdr.processing3_spatial_resolution_unit = None
# 		elif temp[2][0:4] in ["tmin","tmax","tsum"]:
# 			pdr.processing3_temporal_resolution = Decimal(re.sub(r"[A-Za-z]+", '', temp[2][4:99]))
# 			pdr.processing3_temporal_resolution_unit = re.sub(r"[^A-Za-z]+", '', temp[2][4:99])
# 			pdr.processing3_spatial_resolution = None
# 			pdr.processing3_spatial_resolution_unit = None
# 		else:
# 			pdr.processing3 = temp[2]
# 			pdr.processing3_temporal_resolution = None
# 			pdr.processing3_temporal_resolution_unit = None
# 			pdr.processing3_spatial_resolution = None
# 			pdr.processing3_spatial_resolution_unit = None
# 	if not pdr.data_processed:
# 		pdr.processing_string = ""
# 	else:
# 		if pdr.processing1_temporal_resolution:
# 			pdr.processing_string = pdr.processing1 + str('{0:g}'.format(float(pdr.processing1_temporal_resolution)) + str(pdr.processing1_temporal_resolution_unit)
# 		elif pdr.processing1_spatial_resolution:
# 			pdr.processing_string = pdr.processing1 + str('{0:g}'.format(float(pdr.processing1_spatial_resolution))) + str(pdr.processing1_spatial_resolution_unit)
# 		else:
# 			pdr.processing_string = pdr.processing1
# 		if (pdr.processing2):
# 			if pdr.processing2_temporal_resolution:
# 				pdr.processing_string = pdr.processing_string + "_" + pdr.processing2 + str('{0:g}'.format(float(pdr.processing2_temporal_resolution))) + str(pdr.processing2_temporal_resolution_unit)
# 			elif pdr.processing2_spatial_resolution:
# 				pdr.processing_string = pdr.processing_string + "_" + pdr.processing2 + str('{0:g}'.format(float(pdr.processing2_spatial_resolution))) + str(pdr.processing2_spatial_resolution_unit)
# 			else:
# 				pdr.processing_string = pdr.processing_string + "_" + pdr.processing2
# 			if (pdr.processing3):
# 				if pdr.processing3_temporal_resolution:
# 					pdr.processing_string = pdr.processing_string + "_" + pdr.processing3 + str('{0:g}'.format(float(pdr.processing3_temporal_resolution))) + str(pdr.processing3_temporal_resolution_unit)
# 				elif pdr.processing3_spatial_resolution:
# 					pdr.processing_string = pdr.processing_string + "_" + pdr.processing3 + str('{0:g}'.format(float(pdr.processing3_spatial_resolution))) + str(pdr.processing3_spatial_resolution_unit)
# 				else:
# 					pdr.processing_string = pdr.processing_string + "_" + pdr.processing3
# 	if pdr.processing_string != pdr.processing_string_original:
# 		raise ValidationError("Newly generated processing string does not match old one - new: "+pdr.processing_string+" | old: "+pdr.processing_string_original)
# 	data_root = "/net/exo/landclim/data/dataset"
# 	if not pdr.dataset_realization:
# 		raise ValidationError("SubDataset missing for PDR "+str(pdr))
# 		if not pdr.dataset_realization.subdataset:
# 			raise ValidationError("SubDataset missing for PDR "+str(pdr))
# 	data_path = os.path.join(data_root, pdr.dataset_realization.subdataset.supdataset.short_name.replace(' ','-'))
# 	if pdr.dataset_realization.subdataset.name != None and pdr.dataset_realization.subdataset.name != "":
# 		data_path = data_path + "_" + pdr.dataset_realization.subdataset.name.replace(' ','-')
# 	if (pdr.dataset_realization.subdataset.processing_level!=None and pdr.dataset_realization.subdataset.processing_level!=''):
# 		data_path = data_path + "_L" + pdr.dataset_realization.subdataset.processing_level.replace(' ','-')
# 	if (pdr.dataset_realization.subdataset.band!=None and pdr.dataset_realization.subdataset.band!=''):
# 		data_path = data_path + "_" + pdr.dataset_realization.subdataset.band.replace(' ','-') + "-BAND"
# 	if (pdr.dataset_realization.subdataset.orbit_direction!=None and pdr.dataset_realization.subdataset.orbit_direction!=''):
# 		data_path = data_path + "_" + pdr.dataset_realization.subdataset.orbit_direction.replace(' ','-')
# 	if pdr.dataset_realization.subdataset.version!=None and pdr.dataset_realization.subdataset.version!='':
# 		data_path = os.path.join(data_path, pdr.dataset_realization.subdataset.version.replace(' ','-').lower())
# 	else:
# 		data_path = os.path.join(data_path, str(pdr.dataset_realization.subdataset.download_date).replace('-',''))
# 	doc_path = os.path.join(data_path, "doc")
# 	if len(pdr.dataset_realization.subdataset.link_location)==0:
# 		if (pdr.dataset_realization.is_model==True):
# 			data_path = os.path.join(data_path, "model")
# 		elif (pdr.dataset_realization.is_mixed==True):
# 			data_path = os.path.join(data_path, "mixed")
# 		else:
# 			if (pdr.dataset_realization.spatial_resolution_unit!=None and pdr.dataset_realization.spatial_resolution_unit!='' and len(pdr.dataset_realization.spatial_resolution_unit)>0):
# 				data_path = os.path.join(data_path, str('{0:g}'.format(float(pdr.dataset_realization.spatial_resolution_lat))))
# 				if (pdr.dataset_realization.spatial_resolution_lat != pdr.dataset_realization.spatial_resolution_lon):
# 					data_path = data_path + "x" + str('{0:g}'.format(float(pdr.dataset_realization.spatial_resolution_lon)))
# 				data_path = data_path + pdr.dataset_realization.spatial_resolution_unit.replace(' ','-').replace('°','deg').lower()
# 			elif pdr.dataset_realization.country_scale==True:
# 				data_path = os.path.join(data_path, "country-scale")
# 			elif pdr.dataset_realization.basin_scale==True:
# 				data_path = os.path.join(data_path, "basin-scale")
# 			elif pdr.dataset_realization.point_scale==True:
# 				data_path = os.path.join(data_path, "point-scale")
# 			data_path = data_path + "_" + pdr.dataset_realization.grid.name.replace(' ','-').lower() + "_"
# 			if (pdr.dataset_realization.temporal_resolution_unit!=None and pdr.dataset_realization.temporal_resolution_unit!=''):
# 				data_path = data_path + str('{0:g}'.format(float(pdr.dataset_realization.temporal_resolution))) + pdr.dataset_realization.temporal_resolution_unit.replace(' ','-').lower()
# 			elif pdr.dataset_realization.irregular==True:
# 				data_path = data_path + "irregular"
# 			elif pdr.dataset_realization.time_invariant==True:
# 				data_path = data_path + "time-invariant"
# 		if not pdr.data_processed:
# 			data_path = os.path.join(data_path,"original")
# 		else:
# 			data_path = os.path.join(data_path,"processed", pdr.processing_string.replace(' ','-').lower())
# 	pdr.path_to_data_directory_dataset = data_path
# 	pdr.path_to_data_directory_variable = "/".join(data_path.split("/")[0:6]).replace("/dataset", "/variable/$VARIABLE")	
# 	dir_exists = os.path.exists(pdr.path_to_data_directory_dataset.replace('/net/exo/landclim/data/dataset','/nfs/exo/landclim/dataset'))
# 	pdr.full_clean()
# 	pdr.save()
# 	inc = inc+1


# # make some additional checks
# if ProcessedDatasetRealization.objects.count() != DatasetRealization.objects.count():
# 	raise ValidationError("The number of processed DRS has to be identical to the number of DRs!")
#
# drs = DatasetRealization.objects.all()
# for dr in drs:
# 	if not dr.subdataset:
# 		ValidationError("NO SUB")


# merge dataset realizations that effectively belong together (1 DatasetRealization contains 1 or more ProcessedDatasetRealizations, but each DatasetRealization must be unique)
# subs = SubDataset.objects.all()
# sub = subs[0]
# for sub in subs[0:len(subs)]:
# 	if sub.datasetrealization_set.count()>1:
# 		drkeys = sub.datasetrealization_set.values_list('key')
# 		drkeys_unique, indices_unique = np.unique(drkeys, return_index=True)
# 		drkey_unique = drkeys_unique[0]
# 		for drkey_unique in drkeys_unique:
# 			drs = DatasetRealization.objects.filter(subdataset=sub).filter(key=drkey_unique)
# 			if drs.count() > 1:
# 				dr_keep = drs[0]
# 				dr_remove = drs[1:drs.count()][0]
# 				for dr_remove in drs[1:drs.count()]:
# 					if dr_remove.processeddatasetrealization_set.count() > 0:
# 						pdr_keep = dr_remove.processeddatasetrealization_set.all()[0]
# 						dr_remove.processeddatasetrealization_set.remove(pdr_keep)
# 						dr_remove.processeddatasetrealization_set.clear()
# 						p_dummy = ProcessedDatasetRealization.objects.create()
# 						dr_remove.processeddatasetrealization_set.add(p_dummy)
# 						p_dummy.save()
# 						dr_keep.processeddatasetrealization_set.add(pdr_keep)
# 						dr_keep.save()
# 						dr_remove.delete()
# 						p_dummy.delete()

# # after successful migration, add a unique constraint to the field DatasetRealization.key
# drs = DatasetRealization.objects.all()
# drkeys = DatasetRealization.objects.values_list("key")
# drkeys_duplicated = [item for item, count in Counter(drkeys).items() if count > 1]
# if len(drkeys_duplicated) > 0:
# 	raise ValidationError("Duplicate dataset realization keys detected - this violates the unique constraint!")

# # after successful migration, ensure that invalid fields are successfully renamed
# pdrs = ProcessedDatasetRealization.objects.all()
# vals = []
# vals_toremove = ["param","tsub1950-2009"]
# for pdr in pdrs:
# 	if (pdr.processing1 in vals_toremove) or (pdr.processing2 in vals_toremove) or (pdr.processing3 in vals_toremove):
# 		raise ValidationError("wrong param detected")
# 	if pdr.processing1:
# 		vals.append(pdr.processing1)
# 	if pdr.processing2:
# 		vals.append(pdr.processing2)
# 	if pdr.processing3:
# 		vals.append(pdr.processing3)

# print(np.unique(np.sort(vals)))

# pdrs = ProcessedDatasetRealization.objects.all()
# vals = []
# vals_toremove = ["y ltm"]
# for pdr in pdrs:
# 	if (pdr.processing1_temporal_resolution_unit in vals_toremove) or (pdr.processing2_temporal_resolution_unit in vals_toremove) or (pdr.processing3_temporal_resolution_unit in vals_toremove):
# 		raise ValidationError("wrong param detected")
# 	if pdr.processing1_temporal_resolution_unit:
# 		vals.append(pdr.processing1_temporal_resolution_unit)
# 	if pdr.processing2_temporal_resolution_unit:
# 		vals.append(pdr.processing2_temporal_resolution_unit)
# 	if pdr.processing3_temporal_resolution_unit:
# 		vals.append(pdr.processing3_temporal_resolution_unit)

# print(np.unique(np.sort(vals)))

# drs = DatasetRealization.objects.all()
# vals = []
# vals_toremove = ["y ltm"]
# for dr in drs:
# 	if (dr.temporal_resolution_unit in vals_toremove):
# 		raise ValidationError("wrong param detected")
# 	if dr.temporal_resolution_unit:
# 		vals.append(dr.temporal_resolution_unit)

# print(np.unique(np.sort(vals)))

# sups = SupDataset.objects.all()
# subs = SubDataset.objects.all()
# categories_valid=['digital elevation model','disaster','land cover / land use','model output','composite','in situ','in situ gridded','remote sensing','diagnostic / index','population','reanalysis']
# categories_all=[]
# for sup in sups:
# 	categories_all.append(sup.data_origin_category)
# 	if sup.data_origin_category == "":
# 		sup.data_origin_category = None
# 	if sup.data_origin_category == "other primary datasets":
# 		sup.data_origin_category = "composite"
# 	if sup.data_origin_category == "remote sensing data":
# 		sup.data_origin_category = "remote sensing"
# 	if sup.data_origin_category == "diagnostic":
# 		sup.data_origin_category = "diagnostic / index"
# 	if sup.data_origin_category == "reanalysis derived":
# 		sup.data_origin_category = "reanalysis"
# 	if sup.data_origin_category == "in-situ, remote sensing":
# 		sup.data_origin_category = "in situ"
# 	if sup.data_origin_category == "in-situ":
# 		sup.data_origin_category = "in situ"
# 	if sup.data_origin_category == "in-situ, satellite data":
# 		sup.data_origin_category = "in situ gridded"
# 	if sup.data_origin_category == "gaging stations":
# 		sup.data_origin_category = "in situ"
# 	if sup.data_origin_category == "in-situ gridded":
# 		sup.data_origin_category = "in situ gridded"
# 	if sup.data_origin_category == "crop planting andharvesting dates, CRU CL 2.0":
# 		sup.data_origin_category = "land cover / land use"
# 	if sup.data_origin_category == "in-situ and model data":
# 		sup.data_origin_category = "in situ gridded"
# 	if sup.data_origin_category == "gridded observational data":
# 		sup.data_origin_category = "in situ gridded"
# 	if sup.data_origin_category == "disasters":
# 		sup.data_origin_category = "disaster"
# 	if sup.data_origin_category == "atmospheric reanalysis":
# 		sup.data_origin_category = "reanalysis"
# 	if sup.data_origin_category == "Simulations where the ERA-Interim near-surface meteorological forcing is used with the latest version of the ECMWF land surface model.":
# 		sup.data_origin_category = "model output"
# 	if sup.data_origin_category == "flux tower station data":
# 		sup.data_origin_category = "in situ"
# 	if sup.data_origin_category == "gridded in-situ":
# 		sup.data_origin_category = "in situ gridded"
# 	if sup.data_origin_category == "land cover data":
# 		sup.data_origin_category = "land cover / land use"
# 	if sup.data_origin_category == "model data":
# 		sup.data_origin_category = "model output"
# 	if sup.data_origin_category == "model data, remote sensing":
# 		sup.data_origin_category = "model output"
# 	if sup.data_origin_category == "in-situ observations, gaging stations":
# 		sup.data_origin_category = "in situ"
# 	if sup.data_origin_category == "gaging stations and remote sensing":
# 		sup.data_origin_category = "in situ gridded"
# 	if sup.data_origin_category == "population data":
# 		sup.data_origin_category = "population"
# 	if sup.data_origin_category == "global soil data":
# 		sup.data_origin_category = "in situ gridded"
# 	if sup.data_origin_category == "DEM":
# 		sup.data_origin_category = "digital elevation model"
# 	if sup.data_origin_category == "in situ temperature observations":
# 		sup.data_origin_category = "in situ"
# 	if sup.data_origin_category == "historical data":
# 		sup.data_origin_category = "in situ"
# 	if sup.data_origin_category == "historical data ":
# 		sup.data_origin_category = "in situ"
# 	if sup.data_origin_category == "in-situ synthesis":
# 		sup.data_origin_category = "in situ gridded"
# 	if sup.data_origin_category == "agricultural statistics, areas equipped for irrigation, cropland extent, harvested areas, administrative boundaries":
# 		sup.data_origin_category = "land cover / land use"
# 	if sup.data_origin_category == "in situ synthesis":
# 		sup.data_origin_category = "in situ gridded"
# 	if sup.data_origin_category == "model data forced by observation":
# 		sup.data_origin_category = "model output"
# 	if sup.data_origin_category == "model data forced by observations":
# 		sup.data_origin_category = "model output"
# 	if sup.data_origin_category == "in situ and map data":
# 		sup.data_origin_category = "in situ gridded"
# 	if sup.data_origin_category == "indices":
# 		sup.data_origin_category = "diagnostic / index"
# 	if sup.data_origin_category == "adjusted reanalysis":
# 		sup.data_origin_category = "reanalysis"
# 	if sup.data_origin_category == "based on artificial neural networks":
# 		sup.data_origin_category = "in situ gridded"
# 	if sup.data_origin_category == "multi-model simulations":
# 		sup.data_origin_category = "model output"
# 	if sup.data_origin_category:
# 		if not sup.data_origin_category in categories_valid:
# 			raise ValidationError("Incorrect data category: "+sup.data_origin_category)
# 	sup.save()

# categories_all = np.unique(categories_all)

# categories_all=[]
# for sub in subs:
# 	categories_all.append(sub.data_origin_category)
# 	if sub.data_origin_category == "":
# 		sub.data_origin_category = None
# 	if sub.data_origin_category == "other primary datasets":
# 		sub.data_origin_category = "composite"
# 	if sub.data_origin_category == "remote sensing data":
# 		sub.data_origin_category = "remote sensing"
# 	if sub.data_origin_category == "diagnostic":
# 		sub.data_origin_category = "diagnostic / index"
# 	if sub.data_origin_category == "reanalysis derived":
# 		sub.data_origin_category = "reanalysis"
# 	if sub.data_origin_category == "in-situ, remote sensing":
# 		sub.data_origin_category = "in situ"
# 	if sub.data_origin_category == "in-situ":
# 		sub.data_origin_category = "in situ"
# 	if sub.data_origin_category == "in-situ, satellite data":
# 		sub.data_origin_category = "in situ gridded"
# 	if sub.data_origin_category == "gaging stations":
# 		sub.data_origin_category = "in situ"
# 	if sub.data_origin_category == "in-situ gridded":
# 		sub.data_origin_category = "in situ gridded"
# 	if sub.data_origin_category == "crop planting andharvesting dates, CRU CL 2.0":
# 		sub.data_origin_category = "land cover / land use"
# 	if sub.data_origin_category == "in-situ and model data":
# 		sub.data_origin_category = "in situ gridded"
# 	if sub.data_origin_category == "gridded observational data":
# 		sub.data_origin_category = "in situ gridded"
# 	if sub.data_origin_category == "disasters":
# 		sub.data_origin_category = "disaster"
# 	if sub.data_origin_category == "atmospheric reanalysis":
# 		sub.data_origin_category = "reanalysis"
# 	if sub.data_origin_category == "Simulations where the ERA-Interim near-surface meteorological forcing is used with the latest version of the ECMWF land surface model.":
# 		sub.data_origin_category = "model output"
# 	if sub.data_origin_category == "Simulations where the ERA-Interim near-surface meteorological forcing is used with the latest version of the ECMWF land surface model":
# 		sub.data_origin_category = "model output"
# 	if sub.data_origin_category == "flux tower station data":
# 		sub.data_origin_category = "in situ"
# 	if sub.data_origin_category == "gridded in-situ":
# 		sub.data_origin_category = "in situ gridded"
# 	if sub.data_origin_category == "land cover data":
# 		sub.data_origin_category = "land cover / land use"
# 	if sub.data_origin_category == "model data":
# 		sub.data_origin_category = "model output"
# 	if sub.data_origin_category == "model data, remote sensing":
# 		sub.data_origin_category = "model output"
# 	if sub.data_origin_category == "in-situ observations, gaging stations":
# 		sub.data_origin_category = "in situ"
# 	if sub.data_origin_category == "gaging stations and remote sensing":
# 		sub.data_origin_category = "in situ gridded"
# 	if sub.data_origin_category == "population data":
# 		sub.data_origin_category = "population"
# 	if sub.data_origin_category == "global soil data":
# 		sub.data_origin_category = "in situ gridded"
# 	if sub.data_origin_category == "DEM":
# 		sub.data_origin_category = "digital elevation model"
# 	if sub.data_origin_category == "in situ temperature observations":
# 		sub.data_origin_category = "in situ"
# 	if sub.data_origin_category == "historical data":
# 		sub.data_origin_category = "in situ"
# 	if sub.data_origin_category == "historical data ":
# 		sub.data_origin_category = "in situ"
# 	if sub.data_origin_category == "in-situ synthesis":
# 		sub.data_origin_category = "in situ gridded"
# 	if sub.data_origin_category == "agricultural statistics, areas equipped for irrigation, cropland extent, harvested areas, administrative boundaries":
# 		sub.data_origin_category = "land cover / land use"
# 	if sub.data_origin_category == "in situ synthesis":
# 		sub.data_origin_category = "in situ gridded"
# 	if sub.data_origin_category == "model data forced by observation":
# 		sub.data_origin_category = "model output"
# 	if sub.data_origin_category == "model data forced by observations":
# 		sub.data_origin_category = "model output"
# 	if sub.data_origin_category == "in situ and map data":
# 		sub.data_origin_category = "in situ gridded"
# 	if sub.data_origin_category == "indices":
# 		sub.data_origin_category = "diagnostic / index"
# 	if sub.data_origin_category == "adjusted reanalysis":
# 		sub.data_origin_category = "reanalysis"
# 	if sub.data_origin_category == "based on artificial neural networks":
# 		sub.data_origin_category = "in situ gridded"
# 	if sub.data_origin_category == "multi-model simulations":
# 		sub.data_origin_category = "model output"
# 	if sub.data_origin_category == "static data":
# 		sub.data_origin_category = None
# 	if sub.data_origin_category == "land use data":
# 		sub.data_origin_category = "land cover / land use"
# 	if sub.data_origin_category == "satellite":
# 		sub.data_origin_category = "remote sensing"
# 	if sub.data_origin_category == "":
# 		sub.data_origin_category = None
# 	if sub.data_origin_category:
# 		if not sub.data_origin_category in categories_valid:
# 			raise ValidationError("Incorrect data category: "+sub.data_origin_category)
# 	sub.save()

# ensure there are no duplicate entries among properties that are shared among SupDatasets and SubDatasets
# subs=SubDataset.objects.all()
# for sub in subs:
# 	sup = sub.supdataset
# 	print("Processing "+str(sup)+" - "+str(sub))
# 	if sup.summary==sub.summary:
# 		sub.summary=""
# 	if sup.publication_doi==sub.publication_doi:
# 		sub.publication_doi=""
# 	if sup.dataset_doi==sub.dataset_doi:
# 		sub.dataset_doi=""
# 	if sup.project_url==sub.project_url:
# 		sub.project_url=""
# 	if sup.data_source_url==sub.data_source_url:
# 		sub.data_source_url=""
# 	if sup.known_issues==sub.known_issues:
# 		sub.known_issues=""
# 	if sup.known_issues_link==sub.known_issues_link:
# 		sub.known_issues_link=""
# 	if sup.data_policy==sub.data_policy:
# 		sub.data_policy=""
# 	if sup.data_policy_link==sub.data_policy_link:
# 		sub.data_policy_link=""
# 	if sup.manual_link==sub.manual_link:
# 		sub.manual_link=""
# 	if sup.quick_user_guide_link==sub.quick_user_guide_link:
# 		sub.quick_user_guide_link=""
# 	if sup.technical_manual_link==sub.technical_manual_link:
# 		sub.technical_manual_link=""
# 	if sup.source_dataset==sub.source_dataset:
# 		sub.source_dataset=""
# 	if sup.source_processing==sub.source_processing:
# 		sub.source_processing=""
# 	sub.save()