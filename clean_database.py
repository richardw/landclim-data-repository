#!/webapps/landclimdata/.pyenv/shims/python

import django
import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "landclim_data_repository.settings")
django.setup()

from iacdatarepository.models import Grid, Organization, GeographicRegion, People, VariableGroup, SupDataset, SubDataset, DatasetRealization, ProcessedDatasetRealization


sups = SupDataset.objects.all()
for sup in sups:
	if sup is None or sup.short_name=="" or sup.short_name is None:
		print("Deleting dead SUP, SUBs, DRs and PDRs: "+str(sup.pk))
		subs_to_delete = sup.subdataset_set.all()
		for sub_to_delete in subs_to_delete:
			drs_to_delete = sub_to_delete.datasetrealization_set.all()
			for dr_to_delete in drs_to_delete:				
				pdrs_to_delete = dr_to_delete.processeddatasetrealization_set.all()
				create_dummydir = False
				if pdrs_to_delete.count() > 0:
					create_dummydir = True
					for pdr_to_delete in pdrs_to_delete:
						dir_to_create = pdr_to_delete.path_to_data_directory_dataset.replace("/net/exo","/nfs/exo").replace("/landclim/data","/landclim")
						os.system("mkdir -p "+dir_to_create)
						pdr_to_delete.delete()
				if create_dummydir:
					os.system("mkdir -p "+dir_to_create)
				dr_to_delete.delete()
				if create_dummydir:
					os.system("rm -rf "+dir_to_create)
			sub_to_delete.delete()
		sup.delete()

subs = SubDataset.objects.all()
for sub in subs:
	if sub.supdataset is None:
		print("Deleting dead SUB, DRs and PDRs: "+str(sub.pk))
		drs_to_delete = sub.datasetrealization_set.all()
		if drs_to_delete.count() > 0:
			for dr_to_delete in drs_to_delete:
				pdrs_to_delete = dr_to_delete.processeddatasetrealization_set.all()
				create_dummydir = False
				if pdrs_to_delete.count() > 0:
					create_dummydir = True
					for pdr_to_delete in pdrs_to_delete:
						dir_to_create = pdr_to_delete.path_to_data_directory_dataset.replace("/net/exo","/nfs/exo").replace("/landclim/data","/landclim")
						os.system("mkdir -p "+dir_to_create)
						pdr_to_delete.delete()
				if create_dummydir:
					os.system("mkdir -p "+dir_to_create)
				dr_to_delete.delete()
				if create_dummydir:
					os.system("rm -rf "+dir_to_create)
		sub.delete()

drs = DatasetRealization.objects.all()
for dr in drs:
	if dr.subdataset is None:
		print("Deleting dead DR and PDRs: "+str(dr.pk))
		pdrs_to_delete = dr.processeddatasetrealization_set.all()
		create_dummydir = False
		if pdrs_to_delete.count() > 0:
			create_dummydir = True
			for pdr_to_delete in pdrs_to_delete:
				dir_to_create = pdr_to_delete.path_to_data_directory_dataset.replace("/net/exo","/nfs/exo").replace("/landclim/data","/landclim")
				os.system("mkdir -p "+dir_to_create)
				pdr_to_delete.delete()
		if create_dummydir:
			os.system("mkdir -p "+dir_to_create)
		dr.delete()
		if create_dummydir:
			os.system("rm -rf "+dir_to_create)
	elif dr.subdataset.supdataset is None:
		print("Deleting dead SUB, DR and PDRs: "+str(dr.pk))
		sub_to_delete = dr.subdataset
		if sub_to_delete is not None:
			sub_to_delete.delete()
		pdrs_to_delete = dr.processeddatasetrealization_set.all()
		create_dummydir = False
		if pdrs_to_delete.count() > 0:
			create_dummydir = True
			for pdr_to_delete in pdrs_to_delete:
				dir_to_create = pdr_to_delete.path_to_data_directory_dataset.replace("/net/exo","/nfs/exo").replace("/landclim/data","/landclim")
				os.system("mkdir -p "+dir_to_create)
				pdr_to_delete.delete()
		if create_dummydir:
			os.system("mkdir -p "+dir_to_create)
		dr.delete()
		if create_dummydir:
			os.system("rm -rf "+dir_to_create)

pdrs = ProcessedDatasetRealization.objects.all()
for pdr in pdrs:
	if pdr.dataset_realization == None:		
		print("Deleting dead PDR")
		pdr_to_delete = pdr
		dir_to_create = pdr_to_delete.path_to_data_directory_dataset.replace("/net/exo","/nfs/exo").replace("/landclim/data","/landclim")
		os.system("mkdir -p "+dir_to_create)
		pdr_to_delete.delete()
		os.system("rm -rf "+dir_to_create)
	else:
		if pdr.dataset_realization.subdataset == None:
			print("Deleting dead DR and PDRs")
			dr_to_delete = pdr.dataset_realization
			pdrs_to_delete = dr_to_delete.processeddatasetrealization_set.all()
			create_dummydir = False
			if pdrs_to_delete.count() > 0:
				create_dummydir = True
				for pdr_to_delete in pdrs_to_delete:
					dir_to_create = pdr_to_delete.path_to_data_directory_dataset.replace("/net/exo","/nfs/exo").replace("/landclim/data","/landclim")
					os.system("mkdir -p "+dir_to_create)
					pdr_to_delete.delete()
			if create_dummydir:
				os.system("mkdir -p "+dir_to_create)
			dr_to_delete.delete()
			if create_dummydir:
				os.system("rm -rf "+dir_to_create)
		if pdr.dataset_realization.subdataset.supdataset == None:
			print("Deleting dead SUB, DR and PDRs")
			sub_to_delete = pdr.dataset_realization.subdataset
			if sub_to_delete is not None:
				sub_to_delete.delete()
			dr_to_delete = pdr.dataset_realization
			pdrs_to_delete = dr_to_delete.processeddatasetrealization_set.all()
			create_dummydir = False
			if pdrs_to_delete.count() > 0:
				create_dummydir = True
				for pdr_to_delete in pdrs_to_delete:
					dir_to_create = pdr_to_delete.path_to_data_directory_dataset.replace("/net/exo","/nfs/exo").replace("/landclim/data","/landclim")
					os.system("mkdir -p "+dir_to_create)
					pdr_to_delete.delete()
			if create_dummydir:
				os.system("mkdir -p "+dir_to_create)
			dr_to_delete.delete()
			if create_dummydir:
				os.system("rm -rf "+dir_to_create)
